var express = require('express'); // ExperssJS Framework
var app = express(); // Invoke express to variable for use in application
var port = process.env.PORT || 80; 
var morgan = require('morgan');
var mongoose = require('mongoose'); 
var bodyParser = require('body-parser'); 
var multer = require('multer');
var request = require('request');

// var http = require('http').Server(app);
// var io = require('socket.io')(http);

require('./app/routes/Mod.CrSc.js')();


// var parammmm = require('request-param')
// var path = require('path'); // Import path module
// var passport = require('passport'); // Express-compatible authentication middleware for Node.js.
// var social = require('./app/passport/passport')(app, passport); // Import passport.js End Points/API
app.use(multer({dest: './app/public/uploads/loker/',limits:{fileSize:10000000}}));
app.use(morgan('dev')); // Morgan Middleware
// app.use(bodyParser.json()); // Body-parser middleware
// app.use(bodyParser.urlencoded({ extended: true })); // For parsing application/x-www-form-urlencoded
app.use(express.static(__dirname + '/app')); // Allow front end to access public folder

// var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

var router = express.Router(); // Invoke the Express Router
var appAlumni = require('./app/routes/mod_alumniif')(router); 
app.use('/mod_alumniif', appAlumni); 

var berita = express.Router();
var appBerita = require('./app/routes/mod_berita')(berita); 
app.use('/mod_berita', appBerita); 

var cronalm = express.Router();
var appCronAlm = require('./app/routes/mod_cronalm')(cronalm); 
app.use('/mod_cronalm', appCronAlm); 

var kerja = express.Router();
var appPekerjaan = require('./app/routes/mod_pekerjaan')(kerja); 
app.use('/mod_pekerjaan', appPekerjaan); 

var tdynews = express.Router();
var appTdynews = require('./app/routes/mod_tdynews')(tdynews); 
app.use('/mod_tdynews', appTdynews);

var groupalm = express.Router();
var appGroup = require('./app/routes/mod_group')(groupalm); 
app.use('/mod_mygroup', appGroup);

var forumalm = express.Router();
var appForum = require('./app/routes/mod_forum')(forumalm); 
app.use('/mod_forum', appForum);

var eventalm = express.Router();
var appEvent = require('./app/routes/mod_event')(eventalm); 
app.use('/mod_event', appEvent);

var momalm = express.Router();
var appMom = require('./app/routes/mod_mom')(momalm); 
app.use('/mod_mom', appMom);

var surveyalm = express.Router();
var appSurveyALM = require('./app/routes/mod_survey')(surveyalm); 
app.use('/mod_survey', appSurveyALM);

var hasilsurveyalm = express.Router();
var appHasilSurveyALM = require('./app/routes/mod_hasil_survey')(hasilsurveyalm); 
app.use('/mod_hasil_survey', appHasilSurveyALM);

var surveypemimpin = express.Router();
var appSurveyPMP = require('./app/routes/mod_surveypemimpin')(surveypemimpin); 
app.use('/mod_surveypemimpin', appSurveyPMP);

var hasilsurveypemimpin = express.Router();
var appHasilSurveyPMP = require('./app/routes/mod_hasil_surveypemimpin')(hasilsurveypemimpin); 
app.use('/mod_hasil_surveypemimpin', appHasilSurveyPMP);

// <---------- CONFIGURE SUPERADMIN ---------->
var MgsSuperadmin = express.Router();
var appSuperadmin = require('./app/routes/mods_superadminx')(MgsSuperadmin); 
app.use('/mods_superadminx', appSuperadmin);

var alumnisrvy = express.Router();
var appAlmSrvy = require('./app/routes/mod_almsurvey')(alumnisrvy); 
app.use('/mod_almsurvey', appAlmSrvy);

var alumnihasilsrvy = express.Router();
var appAlmHasilSrvy = require('./app/routes/mod_almhasilsurvey')(alumnihasilsrvy); 
app.use('/mod_almhasilsurvey', appAlmHasilSrvy);

var pimpinansrvy = express.Router();
var appPimpinanSrvy = require('./app/routes/mod_pimpinansurvey')(pimpinansrvy); 
app.use('/mod_pimpinansurvey', appPimpinanSrvy);

var pimpinanhasilsrvy = express.Router();
var appPimpinanHasilSrvy = require('./app/routes/mod_pimpinanhasilsurvey')(pimpinanhasilsrvy); 
app.use('/mod_pimpinanhasilsurvey', appPimpinanHasilSrvy);

var slider = express.Router();
var appSlider = require('./app/routes/mod_slider')(slider); 
app.use('/mod_slider', appSlider);

var scholar = express.Router();
var appScholar = require('./app/routes/mod_scholar')(scholar); 
app.use('/mod_scholar', appScholar);

var strp = express.Router();
var appStartup = require('./app/routes/mod_start')(strp); 
app.use('/mod_start', appStartup);


// <---------- REPLACE WITH YOUR MONGOOSE CONFIGURATION ---------->
// mongodb://omlegron:bugz01@ds135812.mlab.com:35812/mg_alumni //mongodb://localhost:27017/alumni
mongoose.connect('mongodb://127.0.0.1:27017/alumni', function(err) {
    if (err) {
        console.log('Not connected to the database: ' + err); // Log to console if unable to connect to database
    } else {
        console.log('Successfully connected to MongoDB'); // Log to console if able to connect to database
    }
});

// Set Application Static Layout
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/app/index.html')); // Set index.html as layout
});

CronScrap();
CronAlumni();
CronEducation();
CronScholar();

// app.use(require('request-param'));
// io.on('connection', function(socket){
// 	console.log('Koneksi Tersambung');
// 	socket.on('disconnect', function(){
// 		console.log('Koneksi Terputus');
// 	});
// });



// // Start Server
// http.listen(port, function() {
//     console.log('Running the server on port ' + port); // Listen on configured port
// });

app.listen(port, function() {
    console.log('Running the server on port ' + port); // Listen on configured port
});