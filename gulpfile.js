var gulp = require('gulp');
var uglify = require('gulp-uglify');
var pump = require('pump');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');

const minify = require("gulp-babel-minify");
 

 
gulp.task('default', () =>
    gulp.src('./app/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        // .pipe(concat('all.js'))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./app/dist'))
);

gulp.task("minify", () =>
  gulp.src("./app/js/**/*.js")
    .pipe(minify({
      mangle: {
        keepClassName: true
      }
    }))
    .pipe(gulp.dest("./app/js"))
);

gulp.task('watch',function(){
	var Watcher= gulp.watch('./app/js/*.js');
	Watcher.on('change', function(event){
		console.log('File Change' + event.path + 'Changed');
	})	
});

// gulp.task('uglify', function(){
// 	gulp.src('./app/js/*.js')
// 	.pipe(uglify())
// 	.pipe(gulp.dest('./app/dist'));
// });

gulp.task('compress', function (cb) {
  pump([
        gulp.src('./app/dist/**/*.js'),
        uglify(),
        gulp.dest('./app/dist')
    ],
    cb
  );
});

// gulp.task('default', function(){
// 	console.log('GULP RUNNING');
// });