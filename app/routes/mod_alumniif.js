var Alumni      = require('../models/alumni/alumni');
var jwt         = require('jsonwebtoken');
var secret      = 'MgAlmIF01';
var fs          = require('fs');
var request = require('request');
var md5 = require('md5');

module.exports = function(router) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //
//ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //ALUMNI GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// THIS FOR ALUMNI LOGIN
router.post('/loginalumni', function(req, res, err){
  var obj = {}
  obj.tusrNama = req.body.tusrNama;
  obj.tusrPassword = md5(req.body.tusrPassword);
  var dataalm = JSON.stringify(obj);
   var options1 = {url: 'http://103.55.33.52:3000/api/alm/login',method: 'POST',headers: {'content-type' : 'application/json'}, body:dataalm}
     request(options1, function(error, response, body){ 
      console.log('bbbbbpopooooo',body)
            // if (error) {console.log('eros',error); return false;}
     
      if ((error) || (JSON.parse(body).nim === undefined)){
        console.log(error)
         Alumni.findOne({ nim: req.body.tusrNama }).select('nim password active verifi level _id').exec(function(err, hasil){
           console.log('cekERR',err)
           console.log('hasil',hasil)
           if (hasil){
              if (hasil.active === false) {
                res.json({ success: false, message: 'Maaf Akun Anda Belum DI Konfirmasi Oleh Admin'});
                return false;
              }else if (hasil.comparePassword(req.body.tusrPassword) === true){
                res.json({success: true, message: 'Sukses Login', hasil: hasil});
                return true;        
              }else{
                res.json({success: false, message:'Maaf Akun Anda Belum Teregristasi Dengan Benar'});                
                return false;
              }
           }else{
               res.json({success: false, message:'Cek Ulang Kembali Akun Anda'});                
              return false;
           }
        });
      }else{
       
        var hasil = JSON.parse(body);
         console.log('sukses',body)
        res.json({success: true, message:'Sukses Login', hasil:hasil});                
        // return true;
      }
    });
});

// GET ONE ALUMNI
router.get('/getonealumni/:nim', function(req,res){
  var getOne = req.params.nim;
  Alumni.findOne({ nim: getOne}, function(err, cralm){
   if (err){
    res.json({success:false, message: 'Error'});  
    console.log('ERROR', err);
  }
  if (!cralm){
    res.json({ success: false, message:'Alumni Tidak Ditemukan'});
  }else{
    res.json({ success:true, cralm:cralm });     
  }
});
});


// THIS FOR ALUMNI CREATE
router.post('/almcreate', function(req, res){
  var alumni = new Alumni();

  
  Alumni.findOne({ nim: req.body.nim }).select('nim').exec(function(err, alum){
    if (alum){
      res.json({ success: true, message: 'Nim Sudah Tersedia'});
    }else{
       if (req.body.password == null || req.body.password == '' || req.body.nim == null || req.body.nim == '')
        {
          res.json({success: false, message: 'Cek Kembali, Terdapat Data Yang Kosong'});
        }else{
          alumni.password = req.body.password;
          alumni.nim = req.body.nim;              
          alumni.temporarytoken = jwt.sign({nim: alumni.nim}, secret);
          alumni.save(function(err){
            if (!err) {
              res.json({success: true, message: 'Sukses Telah Di Daftarkan'});
              return alumni;
            }else if (err.errors != null){
              if (err.errors.password){
                res.json({success: false, message: err.errors.password.message});    
                return alumni;
              }
            }else if (err){
              if (err.code == 11000){
                console.log(err)
                console.log(err.errmsg[53])
                if ((err.errmsg[52] === "$") && (err.errmsg[53] === "n")){
                  res.json({success: false, message:'Nim Sudah Ada'});                
                }
              }
              return alumni;
            }else{
             res.json({success: false, message: err});    
             return alumni;  
           }
         });
        }     
          }
  });
 
});

//auth LOGin NIM ALUMNI
router.post('/checklognim', function(req, res){
  Alumni.findOne({ nim: req.body.nim }).select('nim').exec(function(err, alumni){
    // 

    if (alumni){
      res.json({ success: true, message: 'Nim Benar'});
    }else{
      res.json({ success: false, message: 'Nim Tidak Ada'});        
    }
  });
});

//auth username ALUMNI
router.post('/checknim', function(req, res){
  Alumni.findOne({ nim: req.body.nim }).select('nim').exec(function(err, alumni){
    // 

    if (alumni){
      res.json({ success: false, message: 'Nim Sudah Tersedia'});
    }else{
      res.json({ success: true, message: 'Nim Dapat Di Daftarkan'});        
    }
  });
});

//auth username ALUMNI
router.post('/checkusername', function(req, res){
  Alumni.findOne({ username: req.body.username }).select('username').exec(function(err, alumni){
    // 

    if (alumni){
      res.json({ success: false, message: 'Username Sudah Tersedia '});
    }else{
      res.json({ success: true, message: 'Username Dapat Di Daftarkan'});        
    }
  });
});

//auth username ALUMNI
router.post('/checkpassword', function(req, res){
 Alumni.findOne({ _id: req.body._id }).select('nim password active verifi level _id').exec(function(err, hasil){
  var validPassword = hasil.comparePassword(req.body.password);
  console.log(validPassword)
  if (validPassword === true){
          // console.log('Sukses Login', hasil)
          res.json({success: true, message: 'Sukses Password Benar'});
        }else{
          // console.log('Password Salah')
          res.json({success: false, message: 'Password Salah'});
        }  
      });
});

//auth username ALUMNI
router.post('/checkemail', function(req, res){
  Alumni.findOne({ email: req.body.email }).select('email').exec(function(err, alumni){
    // 

    if (alumni){
      res.json({ success: false, message: 'Email Sudah Tersedia'});
    }else{
      res.json({ success: true, message: 'Email Dapat Di Daftarkan'});        
    }
  });
});

// PUT ONE AND UPDATE EVENT *EVENT
router.put('/updtalm', function(req, res){
  var getOne = req.body.id_alm;
  Alumni.findOne({ _id: getOne}, function(err, alm){
    if (!alm){
      res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
      return alm;        
    }else{
          // console.log(req.body)
          alm.nama = req.body.nama;
          alm.hp = req.body.hp;
          alm.email = req.body.email;
          alm.asal = req.body.asal;
          alm.tgl_lahir = req.body.tgl_lahir;
          alm.alamat = req.body.alamat;
          alm.urlimgs = req.body.urlimgs;

          alm.save(function(err){
            if (!err){
              res.json({success:true, message:'Sukses Di Edit!'});
              return alm;                             
            }else{
              res.json({success:false, message:'Gagal Cek Kembali!'});
            }
          });
          return alm;
        }
      });
});

router.get('/getallalumni', function(req, res){
  // Alumni.findOne({username:req.decoded.username}, function(err, mainAlm){
    
  //   if (!mainAlm){
  //     res.json({success: false, message:'No Alumni Found'});
  //   } else{

    Alumni.find({}, function(err, alumnis){
      // 
      // alumnis.findOne({ username: req.decoded.username}, function(err, mainSuperadmin){
        
      if (err){
        res.json({ success: false, message: 'Data Alumni Tidak Dapat Ditemukan'});
      }else{
        res.json({ success:true, alumnis:alumnis});
      }
      // });
    // });
    // }
  });  
  });

router.get('/getallcfalm', function(req, res){
  Alumni.find({active:false}, function(err, alumnis){
    // 
      // alumnis.findOne({ username: req.decoded.username}, function(err, mainSuperadmin){
        
      if (err){
        res.json({ success: false, message: 'Data Alumni Tidak Dapat Ditemukan'});
      }else{
        res.json({ success:true, alumnis:alumnis});
      }
      // });
    // });
    // }
  });  
});


// GET ONE ALUMNI
router.get('/getidalumni/:id', function(req,res){
  var getOne = req.params.id;
  Alumni.findOne({ _id: getOne}, function(err, alumni){
   if (err){
    res.json({success:false, message: 'Error'});  
    console.log('ERROR');
  }
  if (!alumni){
    res.json({ success: false, message:'Alumni Tidak Ditemukansss'});
  }else{
    res.json({ success:true, alumni:alumni });     
  }
});
});

// Delete SUPERADMIN
router.delete('/alumnidel/:id', function(req, res){
  var getOne = req.params.id;
  Alumni.findOneAndRemove({ _id:getOne }, function(err, alumni){
    if (alumni){
      res.json({success:true, message: 'Sukses Menghapus'});
      return alumni;
    }else{
      res.json({success:false, message: 'Gagal Menghapus'});
      return alumni;
    }
  });
});

// PUT ONE AND UPDATE EVENT *EVENT
router.put('/updtalmaccount', function(req, res){
  var getOne = req.body._id;
  Alumni.findOne({ _id: getOne}, function(err, alm){
    if (!alm){
      res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
      return alm;        
    }else{
          // console.log(req.body)
          alm.nim = req.body.nim;
            // alm.password = req.body.password;
            alm.save(function(err){
              if (!err) {
                res.json({success: true, message: 'Sukses Telah Di Update'});
                return alm;
              }else if (err){
                if (err.code == 11000){
                  console.log(err.errmsg[52])
                  console.log(err.errmsg[53])
                  if ((err.errmsg[52] === "$") && (err.errmsg[53] === "n")){
                    res.json({success: false, message:'Nim Sudah Ada'});                
                  }
                }
                return alm;
              }else{
               res.json({success: false, message: err});    
               return alm;  
             }
           });
            return alm;
          }
        });
});

// PUT ONE AND UPDATE EVENT *EVENT
router.put('/updtalmaccpass', function(req, res){
  var getOne = req.body._id;
  Alumni.findOne({ _id: getOne}, function(err, alm){
    if (!alm){
      res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
      return alm;        
    }else{
          // console.log(req.body)
          alm.password = req.body.password;
            // alm.password = req.body.password;
            alm.save(function(err){
              if (!err) {
                res.json({success: true, message: 'Sukses Telah Di Update'});
                return alm;
              }else if (err.errors != null){
                if (err.errors.password){
                  res.json({success: false, message: err.errors.password.message});    
                  return alm;
                }
              }else{
               res.json({success: false, message: err});    
               return alm;  
             }
           });
            return alm;
          }
        });
});

// PUT ONE AND KONFIRM AGT  *KONFIRM ANGGOTA
router.put('/confrmyes', function(req, res){
  var getOne = req.body.id_nim;
  var options = {$set: {active: true}};
  Alumni.findOneAndUpdate({ _id: getOne, nim: req.body.nim }, options,{new: true}, function(err, alumni){
    if (alumni){
            // console.log(alumni)
            res.json({success:true, message:'Sukses Mengkonfirmasi'});
            return alumni;
          }else{
            res.json({success:false, message:'Gagal Konfirmasi Anggota'});
            return alumni;  
          }
        });
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //
//ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //ALUMNI PRIORITY MODEL END //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return router;
}