var Gnew        = require('../models/global/Globals.News.js');
var fs          = require('fs');

module.exports = function(berita) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //
//NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //NEWS GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE
berita.post('/newscreate', function(req, res, err){

    if (req.body.addby === undefined){
        var bulk = Gnew.collection.initializeOrderedBulkOp(),
        counter = 0;
        req.body.forEach(function(doc) {
        bulk.find({judul:doc.judul}).upsert().update(
                 {
                   $setOnInsert: { 
                        judul: doc.judul, 
                        perusahaan: doc.perusahaan, 
                        lokasi : doc.lokasi,
                        gajih :  doc.gajih,
                        urlimage: doc.urlimage, 
                        urlslug: doc.urlslug
                        // detail:{
                        //   "deskripsi" : doc.detail['deskripsi'],
                        //   "prasyarat" : doc.detail['prasyarat'],
                        //   "bidang" : doc.detail['bidang'],
                        //   "date_on" : doc.detail['date_on'],
                        //   "date_off" : doc.detail['date_off']
                        // }

                    },
                   // $currentDate: { created_at: true },
                   $set: { 
                        // judul: doc.judul, 
                        // perusahaan: doc.perusahaan, 
                        // lokasi : doc.lokasi,
                        // gajih :  doc.gajih,
                        // urlimage: doc.urlimage, 
                        // urlslug: doc.urlslug,
                        detail:{
                          "deskripsi" : doc.detail['deskripsi'],
                          "prasyarat" : doc.detail['prasyarat'],
                          "bidang" : doc.detail['bidang'],
                          "date_on" : doc.detail['date_on'],
                          "date_off" : doc.detail['date_off']
                        }
                    }
                 }
            )
        
        counter++;
        if (counter % 500 == 0) {
            bulk.execute(function(err, rslt) {
               if (err){
                console.log('err from counter', err)
                res.json({success:false, message:'Err From Counter!'});
               }else{
                  console.log('success from counter',rslt)
                  res.json({success:true, message:'Sukses From Counter!'});
                  bulk = Gnew.collection.initializeOrderedBulkOp();
                  counter = 0;
               }
            });
        }
      });

        // Catch any docs in the queue under or over the 500's
        if (counter > 0) {
            bulk.execute(function(err,result) {
               if (err){
                console.log('err',err)
                res.json({success:false, message:'Scrap! Gagal Di simapan!'});
               }else{
                res.json({success:true, message:'Sukses Di Simpan!'});
                console.log('ssuc',result)
               }
            });
        }
    
    }else{
      // console.log(req.body.gajih)
      var objs = {};
        objs.judul= req.body.judul, 
        objs.perusahaan= req.body.perusahaan, 
        objs.lokasi = req.body.lokasi,
        objs.gajih =  [req.body.gajih,req.body.gajih1],
        objs.urlimage= req.body.urlimage, 
        objs.urlslug= req.body.urlslug,
        objs.addby       = req.body.addby,
        objs.nim        = req.body.nim,
            objs.detail={
              "deskripsi" : req.body.deskripsi,
              "prasyarat" : req.body.prasyarat,
              "bidang" : req.body.bidang,
              "date_on" : req.body.date_on,
              "date_off" : req.body.date_off
            }
      console.log(objs)
        Gnew.create(objs,function(err){
            if (!err){
              res.json({success:true, message:'Sukses Di Simpan!'});
            }else{
              res.json({success:false, message:'Gagal Cek Kembali!'});
            }
         });
    }
});
//GET ALL
berita.get('/getallnews', function(req, res){
  Gnew.find({}, function(err, news){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, news:news});
    }
  })
});

//GET ALL
berita.get('/getallnewsnotby', function(req, res){
  Gnew.find( { addby: { $exists: false }}, function(err, news){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, news:news});
    }
  })
});
//GET ALL
berita.get('/getallnewsby', function(req, res){
  Gnew.find( { addby: { $exists: true }}, function(err, news){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, news:news});
    }
  })
});
//GET ALL WHERE
berita.get('/getallnewsalm/:nim', function(req, res){
  var getOne = req.params.nim;
  Gnew.find({ nim:getOne, addby:'Alumni'}, function(err, news){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, news:news});
    }
  })
});
//GET ONE
berita.get('/gettone/:id', function(req,res){
  var getOne = req.params.id;
  Gnew.findOne({ _id: getOne}, function(err, berita){
    if (err)throw err;
    if (!berita){
      res.json({ success: false, message:'Berita Tidak Ditemukan'});
    }else{
      res.json({ success:true, berita:berita });     
    }
  });
});

// PUT ONE
berita.put('/putonee', function(req, res){
  var getOne =req.body._id;
  // console.log(getOne);
  // console.log(req.files);
  // console.log(req.body.judul);
    Gnew.findOne({ _id: getOne}, function(err, news){
    //   // if (err) throw err;
        if (!news){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});           
          // console.log(req.body);
          // console.log(news);
        }else{
              news.judul = req.body.judul;
              news.perusahaan = req.body.perusahaan;
              news.lokasi = req.body.lokasi;
                news.gajih =  [req.body.gajih,req.body.gajih1],
              news.urlimage = req.body.urlimage;
              news.urlslug = req.body.urlslug;
              news.detail={
                "deskripsi" : req.body.deskripsi,
                "prasyarat" : req.body.prasyarat,
                "bidang" : req.body.bidang,
                "date_on" : req.body.date_on,
                "date_off" : req.body.date_off
              }
              news.save(function(err){
                if (!err){
                      res.json({success:true, message:'Sukses Di Edit!'});
                }else{
                      res.json({success:false, message:'Gagal Cek Kembali!'});
                }
              });
        }
    });
});

// PUT ONE
berita.put('/putoneescrap', function(req, res){
  var getOne = req.body._id;
  
    Gnew.findOne({ _id: getOne}, function(err, news){
    //   // if (err) throw err;
        if (!news){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});           
          // console.log(req.body);
          // console.log(news);
        }else{
          console.log(req.body);
          // console.log(news);
          news.judul = req.body.judul;
          news.perusahaan = req.body.perusahaan;
          news.lokasi = req.body.lokasi;
          news.gajih = req.body.gajih;
          news.detail['deskripsi'] = req.body.deskripsi;
          news.detail['prasyarat'] = req.body.prasyarat;
          news.detail['bidang'] = req.body.bidang;
          // news.detail['date_on'] = req.body.date_on;
          // news.detail['date_off'] = req.body.date_off;
              news.save(function(err){
                if (!err){
                      res.json({success:true, message:'Sukses Di Edit!'});
                }else{
                      res.json({success:false, message:'Gagal Cek Kembali!'});
                }
              });
        }
    });
});

//GET ONE AND DEL
berita.delete('/dellone/:id', function(req, res){
  var getOne = req.params.id;
  Gnew.findOneAndRemove({ _id: getOne}, function(err, berita){
    if (err) throw err;
    if (!berita){
      res.json({ success: false, message:'Gagal Berita Tidak Ditemukan'});
    }else{
      res.json({ success:true, berita:berita });   
        if (berita.imgs !== undefined){
            fs.unlink(berita.imgs, function(error) {
              if (error) {
                  console.log('Error!!');
              }else{
                  console.log('selesai di hapus!!');
              }
            });
          console.log(berita.imgs)
        }else{
          console.log(berita.imgs)
        }
        
    }
  });  
});


return berita;
};