var STARTUPS        = require('../models/startup/startup.js');
var fs          = require('fs');

module.exports = function(strp) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//strp GLOBAL MODEL //strp GLOBAL MODEL //strp GLOBAL MODEL //strp GLOBAL MODEL //strp GLOBAL MODEL //
//strp GLOBAL MODEL //strp GLOBAL MODEL //strp GLOBAL MODEL //strp GLOBAL MODEL //strp GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE strp *strp
strp.post('/create', function(req, res, err){
   STARTUPS.create(req.body,function(err){
          if (!err){
            res.json({success:true, message:'Sukses Di Simpan!'});
          return true;
          }else{
            res.json({success:false, message:'Gagal Cek Kembali!'});
            console.log('errornya', err);
          return false;
          }
       });     
});

//GET ALL STARTUPS *STARTUPS
strp.get('/getall', function(req, res){
  STARTUPS.find({}, function(err, strp){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, strp:strp});
    }
  })
});

//GET FOR STARTUPS Nim *STARTUPS
strp.get('/getallnim/:nim', function(req, res){
  var getOne = req.params.nim;
  STARTUPS.find({nim:getOne},function(err,strp){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, strp:strp});
    }
  }); 
});

//GET FOR STARTUPS ID *STARTUPS
strp.get('/getone/:id', function(req, res){
  var getOne = req.params.id;
  STARTUPS.findOne({ _id:getOne},function(err,strp){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, strp:strp});
    }
  }); 
});

//GET ONE AND DEL STARTUPS *STARTUPS
strp.delete('/dellone/:id', function(req, res){
  var getOne = req.params.id;
  STARTUPS.findOneAndRemove({ _id: getOne}, function(err, strp){
    if (err) throw err;
    if (!strp){
      res.json({ success: false, message:'Gagal Data Tidak Ditemukan'});
      return strp
    }else{
      res.json({ success:true, message:'Sukses Menghapus'});
      return strp   
    }
  });  
});

// PUT ONE AND UPDATE STARTUPS *STARTUPS
strp.put('/updt', function(req, res){
    var getOne = req.body.id;
    STARTUPS.findOne({ _id: getOne}, function(err, edt){
        if (!edt){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return edt;        
        }else{
         
                         
                 edt.nama = req.body.nama,
                edt.produk = req.body.produk,
                edt.bidang = req.body.bidang,
                edt.website = req.body.website,
                edt.tahun = req.body.tahun
                          edt.save(function(err){
                            if (!err){
                                  res.json({success:true, message:'Sukses Di Edit!'});
                              return edt;                             
                            }else{
                              res.json({success:false, message:'Gagal Cek Kembali!'});
                            }
                          });
                          return edt;
                            
                    
        }
    });
});


strp.get('/getallop', function(req, res){
  STARTUPS.find({ $or: [ { addby:'Superadmin' }, { addby: 'Admin' } ] }).exec(function(err, events){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, events:events});
    }
  })
});


strp.get('/getallnotop', function(req, res){
  STARTUPS.find({ $and: [ { addby: {'$ne':'Superadmin'} }, { addby: {'$ne':'Admin'} } ] }).exec(function(err, events){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, events:events});
    }
  })
});




return strp;
};