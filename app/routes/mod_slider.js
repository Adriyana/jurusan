var Sliders        = require('../models/slider/slider.js');
var fs          = require('fs');

module.exports = function(slider) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//slider GLOBAL MODEL //slider GLOBAL MODEL //slider GLOBAL MODEL //slider GLOBAL MODEL //slider GLOBAL MODEL //
//slider GLOBAL MODEL //slider GLOBAL MODEL //slider GLOBAL MODEL //slider GLOBAL MODEL //slider GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE slider *slider
slider.post('/create', function(req, res, err){
   Sliders.create(req.body,function(err){
          if (!err){
            res.json({success:true, message:'Sukses Di Simpan!'});
          return true;
          }else{
            res.json({success:false, message:'Gagal Cek Kembali!'});
            console.log('errornya', err);
          return false;
          }
       });     
});

//GET ALL Sliders *Sliders
slider.get('/getall', function(req, res){
  Sliders.find({}, function(err, slider){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, slider:slider});
    }
  })
});

//GET FOR Sliders ID *Sliders
slider.get('/getone/:id', function(req, res){
  var getOne = req.params.id;
  Sliders.findOne({ _id:getOne},function(err,slider){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, slider:slider});
    }
  }); 
});

//GET ONE AND DEL Sliders *Sliders
slider.delete('/dellone/:id', function(req, res){
  var getOne = req.params.id;
  Sliders.findOneAndRemove({ _id: getOne}, function(err, slider){
    if (err) throw err;
    if (!slider){
      res.json({ success: false, message:'Gagal Sliders Tidak Ditemukan'});
      return slider
    }else{
      res.json({ success:true, message:'Sukses Menghapus'});
      return slider   
    }
  });  
});

// PUT ONE AND UPDATE Sliders *Sliders
slider.put('/updt', function(req, res){
    var getOne = req.body.idd;
    Sliders.findOne({ _id: getOne}, function(err, edt){
        if (!edt){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return edt;        
        }else{
         
                          edt.judul = req.body.judul;
                          edt.imgs = req.body.imgs;
                          edt.deskripsi = req.body.deskripsi;
                          edt.save(function(err){
                            if (!err){
                                  res.json({success:true, message:'Sukses Di Edit!'});
                              return edt;                             
                            }else{
                              res.json({success:false, message:'Gagal Cek Kembali!'});
                            }
                          });
                          return edt;
                            
                    
        }
    });
});


slider.get('/getallop', function(req, res){
  Sliders.find({ $or: [ { addby:'Superadmin' }, { addby: 'Admin' } ] }).exec(function(err, events){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, events:events});
    }
  })
});


slider.get('/getallnotop', function(req, res){
  Sliders.find({ $and: [ { addby: {'$ne':'Superadmin'} }, { addby: {'$ne':'Admin'} } ] }).exec(function(err, events){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, events:events});
    }
  })
});




return slider;
};