var CurveyPmp        = require('../models/pimpinansurvey/RealSurveyPimpinan.js');
var fs          = require('fs');

module.exports = function(pimpinansrvy) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //
//pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //pimpinansrvy GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE pimpinansrvy *pimpinansrvy
pimpinansrvy.post('/create', function(req, res, err){
  // fs.write('pimpinansrvy.json',req.body)
     console.log(req.body)
     CurveyPmp.create(req.body, function(err,cek){
        if (err){
            console.log(err)
            res.json({ success: false, message: 'Data Tidak Ditemukan', cek:cek});
            return cek;
        }else{
            res.json({ success:true});
            return cek;
        }
     })
});

//GET ALL pimpinansrvy *pimpinansrvy
pimpinansrvy.get('/getall', function(req, res){
  CurveyPmp.find({}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ID pimpinansrvy *pimpinansrvy
pimpinansrvy.get('/getids/:id', function(req, res){
  CurveyPmp.findOne({_id:req.params.id}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ARRAY ID_SURVEY pimpinansrvy *pimpinansrvy
pimpinansrvy.post('/getarray', function(req, res){
  if(req.body.length <= 0){
    // console.log(err)
    res.json({ success: false, message: 'Data Tidak Ditemukan'});
  }else{
    console.log(req.body)
    console.log(req.body.length)
    console.log(req.body[0].id_survey)
    var b = []
    for (var i = 0; i < req.body.length; i++){
      var a = req.body[i].id_survey;
      b.push(a)
      console.log('dalam for',b)
    }
    CurveyPmp.find({_id:{'$nin':b}}).exec(function(err, surveys){
      if (err){
        res.json({ success: false, message: 'Data Tidak Ditemukan'});
      }else{
        res.json({ success:true, surveys:surveys});
      }
    })
  }
});

//GET ARRAY ID_SURVEY pimpinansrvy *pimpinansrvy
pimpinansrvy.post('/getarraysoal', function(req, res){
  if(req.body.length <= 0){
    // console.log(err)
    res.json({ success: false, message: 'Data Tidak Ditemukan'});
  }else{
    console.log(req.body)
    console.log(req.body.length)
    console.log(req.body[0].id_survey)
    var b = []
    for (var i = 0; i < req.body.length; i++){
      var a = req.body[i].id_survey;
      b.push(a)
      console.log('dalam for',b)
    }
    CurveyPmp.find({_id:{'$in':b}}).sort({'_id':-1}).exec(function(err, surveys){
      if (err){
        res.json({ success: false, message: 'Data Tidak Ditemukan'});
      }else{
        res.json({ success:true, surveys:surveys});
      }
    })
  }
});

// //GET ALL EVENT WHERE NIM IN USE *EVENT
// pimpinansrvy.get('/getallevntalm/:nim', function(req, res){
//   var getOne = req.params.nim;
//   Event.find({ nim:getOne}, function(err, evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   })
// });

// //GET FOR EVENT ID *EVENT
// pimpinansrvy.get('/getevent/:id', function(req, res){
//   var getOne = req.params.id;
//   Event.findOne({ _id:getOne},function(err,evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   }); 
// });

//GET ONE AND DEL 
pimpinansrvy.delete('/dellone/:id', function(req, res){
  var getOne = req.params.id;
  CurveyPmp.findOneAndRemove({ _id: getOne}, function(err, survey){
    if (err) throw err;
    if (!survey){
      res.json({ success: false, message:'Gagal Data Tidak Ditemukan'});
      return survey
    }else{
      res.json({ success:true});
      return survey   
    }
  });  
});

// PUT ONE AND UPDATE Curvey *Curvey
pimpinansrvy.put('/updtsrvy', function(req, res){
    var getOne = req.body.id_survey;
    CurveyPmp.findOne({ _id: getOne}, function(err, edtsurvey){
        if (!edtsurvey){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return edtsurvey;        
        }else{
            console.log('from req',req.body.soal)
            console.log('from db',edtsurvey.soal)
            
            edtsurvey.soal = req.body.soal,
            edtsurvey.judul = req.body.judul;
            edtsurvey.deskripsi = req.body.deskripsi;
            edtsurvey.save(req.body,function(err){
              if (!err){

                  res.json({success:true, message:'Sukses Di Edit!'});
                  return edtsurvey;                             
              }else{
                  res.json({success:false, message:'Gagal Cek Kembali!'});
              }
            });
          return edtsurvey;
        }
    });
});
// // PUT ONE AND UPDATE CurveyPmp *EVENT
// pimpinansrvy.put('/updtevent', function(req, res){
//     var getOne = req.body.id_group;
//     Event.findOne({ _id: getOne}, function(err, edtevent){
//         if (!edtevent){
//           res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
//           return edtevent;        
//         }else{
//           // console.log('data', req.files);
//           // console.log('data2', req.files.imgs);
//             if (req.files.imgs === undefined){
//                 edtevent.judul = req.body.judul;
//                   // edtevent.imgs = req.files.imgs['path'];
//                   edtevent.postings = req.body.postings;
//                   edtevent.tag = req.body.tag;
//                   edtevent.save(function(err){
//                     if (!err){
//                         res.json({success:true, message:'Sukses Di Edit!'});
//                         return edtevent;                             
//                     }else{
//                       res.json({success:false, message:'Gagal Cek Kembali!'});
//                     }
//                   });
//             }else if(req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
//                     fs.unlink(edtevent.imgs, function(error) {
//                       if (error) {
//                           console.log('Error!!');
//                       }else{    
//                           edtevent.judul = req.body.judul;
//                           edtevent.imgs = req.files.imgs['path'];
//                           edtevent.postings = req.body.postings;
//                           edtevent.tag = req.body.tag;
//                           edtevent.save(function(err){
//                             if (!err){
//                                   res.json({success:true, message:'Sukses Di Edit!'});
//                               return edtevent;                             
//                             }else{
//                               res.json({success:false, message:'Gagal Cek Kembali!'});
//                             }
//                           });
//                           return edtevent;
//                             console.log('SAVE,selesai di hapus!!');
//                       }
//                     });
//             }else{
//                 res.json({ success: false, message: 'Extensi Harus Png/Jpg/Jpeg'});
//                 fs.unlink(req.files.imgs['path'], function(error) {
//                   if (error) {
//                    console.log('Error!!', Error);
//                   }else{
//                     console.log('selesai di hapus!!');
//                   }
//                   });
//             return edtevent;
//           }
//           return edtevent;
//         }
//     });
// });


// pimpinansrvy.get('/getallop', function(req, res){
//   Event.find({ $or: [ { addby:'Superadmin' }, { addby: 'Admin' } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });


// pimpinansrvy.get('/getallnotop', function(req, res){
//   Event.find({ $and: [ { addby: {'$ne':'Superadmin'} }, { addby: {'$ne':'Admin'} } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });




return pimpinansrvy;
};