var Cr          = require('../models/cr/Cr.Alm.js');
var fs          = require('fs');
var request = require('request');


module.exports = function(cronalm) {
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //
//CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //CRALM GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
cronalm.post('/crcreate', function(req, res){
    // var cr = new Cr(req.body);
    // var queryng = req.body;
    // Cr.insertMany(queryng,function(err, datacr){
    //  if(err){
    //    console.log('MAAF DATA YANG DI CRON ERROR CEK RESULTS FILE')
    //    fs.writeFile('results_cralmDb.err.json',err); 
    //    res.json({success: false, message: err});                
    //  }else{
    //   console.log('Data Sukses Dikirim Ke DB');
    //    res.json({success: true, message:'Data Sukses Di Simpan'});                
    //  }
    // });

    // if (req.body.addby === undefined){
        var bulk = Cr.collection.initializeOrderedBulkOp(),
        counter = 0;
        req.body.forEach(function(doc) {
        bulk.find({mhsNiu:doc.mhsNiu}).upsert().update(
                 {
                   $setOnInsert: { 
                    mhsNiu: doc.mhsNiu,
                    mhsNama: doc.mhsNama,
                    mhsJenisKelamin: doc.mhsJenisKelamin,
                    tempatLahir: doc.tempatLahir,
                    mhsTanggalLahir: doc.mhsTanggalLahir,
                    agmrNama: doc.agmrNama
                  },
                    $set: { 
                        // mhsNiu: doc.mhsNiu,
                        mhsJumlahSaudara: doc.mhsJumlahSaudara,
                        mhsAlamatMhs: doc.mhsAlamatMhs,
                        mhsNoHp:doc.mhsNoHp,
                        mhsEmail: doc.mhsEmail,
                        mhsProdiKode:doc.mhsProdiKode,
                        prodiNamaResmi:doc.prodiNamaResmi,
                        mhsAngkatan:doc.mhsAngkatan,
                        mhsTanggalLulus: doc.mhsTanggalLulus,
                        prodi_info: doc.prodi_info,
                        dosen_pa : doc.dosen_pa,
                        nip_pa : doc.nip_pa
                    }
                 }
            )
        
        counter++;
        if (counter % 50000000 == 0) {
            bulk.execute(function(err, rslt) {
               if (err){
                console.log('err from counter', err)
                res.json({success:false, message:'Err From Counter!'});
               }else{
                  console.log('success from counter',rslt)
                  res.json({success:true, message:'Sukses From Counter!'});
                  bulk = Cr.collection.initializeOrderedBulkOp();
                  counter = 0;
               }
            });
        }
      });

        // Catch any docs in the queue under or over the 500's
        if (counter > 0) {
            bulk.execute(function(err,result) {
               if (err){
                console.log('err',err)
                res.json({success:false, message:'Scrap! Gagal Di simapan!'});
               }else{
                res.json({success:true, message:'Sukses Di Simpan!'});
                console.log('ssuc',result)
               }
            });
        }
    
    // }else{
    //   // console.log(req.body.gajih)
    //   var objs = {};
    //     objs.judul= req.body.judul, 
    //     objs.perusahaan= req.body.perusahaan, 
    //     objs.lokasi = req.body.lokasi,
    //     objs.gajih =  [req.body.gajih,req.body.gajih1],
    //     objs.urlimage= req.body.urlimage, 
    //     objs.urlslug= req.body.urlslug,
    //     objs.addby       = req.body.addby,
    //     objs.nim        = req.body.nim,
    //         objs.detail={
    //           "deskripsi" : req.body.deskripsi,
    //           "prasyarat" : req.body.prasyarat,
    //           "bidang" : req.body.bidang,
    //           "date_on" : req.body.date_on,
    //           "date_off" : req.body.date_off
    //         }
    //   console.log(objs)
    //     Cr.create(objs,function(err){
    //         if (!err){
    //           res.json({success:true, message:'Sukses Di Simpan!'});
    //         }else{
    //           res.json({success:false, message:'Gagal Cek Kembali!'});
    //         }
    //      });
    // }
});

cronalm.post('/create', function(req, res){
    // var cr = new Cr(req.body);
    var queryng = req.body;
    console.log('req ', req.body)
    Cr.create(queryng,function(err, datacr){
     if(err){
      // fs.writeFile('results_cralmDb.err.json',err); 
       res.json({success: false, message: err});                
     }else{
      console.log('Data Sukses Dikirim Ke DB');
       res.json({success: true, message:'Data Sukses Di Simpan'});                
     }
    });
});

//GET ONE NIM
cronalm.get('/getonecralm/:mhsNiu', function(req,res){
  var getOne = req.params.mhsNiu;
  Cr.findOne({ mhsNiu: getOne}, function(err, cralm){
    // if (err)throw err;
    if (!cralm){
      res.json({ success: false, message:'Alumni Tidak Ditemukan', data:cralm});
    }else{
      res.json({ success:true, cralm:cralm });     
    }
  });
});

//GET ONE ID
cronalm.get('/getonecralmid/:id', function(req,res){
  var getOne = req.params.id;
  Cr.findOne({ _id: getOne}, function(err, cralm){
    // if (err)throw err;
    // console.log(cralm)
    if (!cralm){
      res.json({ success: false, message:'Alumni Tidak Ditemukanss', data:cralm});
    }else{
      res.json({ success:true, cralm:cralm });     
    }
  });
});


//GET ALL
cronalm.get('/getallcralm', function(req, res){
  Cr.find({}, function(err, cralm){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, cralm:cralm});
    }
  })
});

// PUT ONE AND UPDATE EVENT *EVENT
cronalm.put('/updtcralm', function(req, res){
    var getOne = req.body.id_cralm;
    Cr.findOne({ _id: getOne}, function(err, cralm){
        if (!cralm){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return cralm;        
        }else{
            cralm.mhsNama = req.body.mhsNama;
            cralm.mhsNoHp = req.body.mhsNoHp;
            cralm.mhsEmail = req.body.mhsEmail;
            cralm.tempatLahir = req.body.tempatLahir;
            cralm.mhsTanggalLahir = req.body.mhsTanggalLahir;
            cralm.mhsAlamatMhs = req.body.mhsAlamatMhs;
            cralm.urlimgs = req.body.urlimgs;       
          cralm.save(function(err){
              if (!err){
                  res.json({success:true, message:'Sukses Di Edit!'});
                  return cralm;                             
              }else{
                  res.json({success:false, message:'Gagal Cek Kembali!'});
              }
           });
          return cralm;
        }
    });
  });

// PUT ONE AND UPDATE EVENT *EVENT
cronalm.put('/updtcralmsp', function(req, res){
    var getOne = req.body._id;
    Cr.findOne({ _id: getOne}, function(err, cralm){
        if (!cralm){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return cralm;        
        }else{
          cralm.mhsNama           = req.body.mhsNama;
          cralm.mhsProdiKode      = req.body.mhsProdiKode;
          cralm.prodiNamaResmi    = req.body.prodiNamaResmi;
          cralm.mhsAngkatan       = req.body.mhsAngkatan;
          cralm.mhsTanggalLulus   = req.body.mhsTanggalLulus;
          cralm.prodi_info        = req.body.prodi_info;
          cralm.dosen_pa          = req.body.dosen_pa;
          cralm.nip_pa            = req.body.nip_pa;
          cralm.mhsJenisKelamin   = req.body.mhsJenisKelamin;
          cralm.tempatLahir       = req.body.tempatLahir;
          cralm.mhsTanggalLahir   = req.body.mhsTanggalLahir;
          cralm.agmrNama          = req.body.agmrNama;
          cralm.mhsJumlahSaudara  = req.body.mhsJumlahSaudara;
          cralm.mhsNoHp           = req.body.mhsNoHp;
          cralm.mhsEmail          = req.body.mhsEmail;   
          cralm.mhsAlamatMhs      = req.body.mhsAlamatMhs;
          cralm.save(function(err){
              if (!err){
                  res.json({success:true, message:'Sukses Di Edit!'});
                  return cralm;                             
              }else{
                  res.json({success:false, message:'Gagal Cek Kembali!'});
              }
           });
          return cralm;
        }
    });
  });

// Delete CRALM
cronalm.delete('/cralmdel/:id', function(req, res){
  var getOne = req.params.id;
  Cr.findOneAndRemove({ _id:getOne }, function(err){
      if (err){
        res.json({success:false, message: 'Gagal Menghapus'});
      }else{
        res.json({ success:true });
      }
  });
});

return cronalm;
}