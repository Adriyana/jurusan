var Event        = require('../models/event/EventAlm.js');
var fs          = require('fs');

module.exports = function(eventalm) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Event GLOBAL MODEL //Event GLOBAL MODEL //Event GLOBAL MODEL //Event GLOBAL MODEL //Event GLOBAL MODEL //
//Event GLOBAL MODEL //Event GLOBAL MODEL //Event GLOBAL MODEL //Event GLOBAL MODEL //Event GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE EVENT *EVENT
eventalm.post('/create', function(req, res, err){
   if (req.body.addby === undefined){
        console.log('test')
        var bulk = Event.collection.initializeOrderedBulkOp(),
        counter = 0;
        req.body.forEach(function(doc) {
        bulk.find({judul:doc.judul}).upsert().update(
                 {
                   $setOnInsert: { 
                        judul: doc.judul, 
                        urlslug:doc.urlslug,
                        imgs: doc.urlimage, 
                        addby: 'Admin'
                    },
                   // $currentDate: { created_at: true },
                   $set: { 
                        postings:doc.detail['deskripsi']
                    }
                 }
            )
        
        counter++;
        if (counter % 500 == 0) {
            bulk.execute(function(err, rslt) {
               if (err){
                console.log('err from counter', err)
                res.json({success:false, message:'Err From Counter!'});
               }else{
                  console.log('success from counter',rslt)
                  res.json({success:true, message:'Sukses From Counter!'});
                  bulk = Event.collection.initializeOrderedBulkOp();
                  counter = 0;
               }
            });
        }
      });

        // Catch any docs in the queue under or over the 500's
        if (counter > 0) {
            bulk.execute(function(err,result) {
               if (err){
                console.log('errEvent',err)
                // res.json({success:false, message:'Scrap! Gagal Di simpan!'});
               }else{
                // res.json({success:true, message:'Sukses Di Simpan!'});
                console.log('ssucEvent',result)
               }
            });
        }
    
    }else{
       var evnt = {};
       evnt.judul = req.body.judul;
       evnt.urlslug = req.body.urlslug;
       evnt.postings = req.body.postings;
       evnt.tag = req.body.tag;
       evnt.imgs = req.body.imgs;
       evnt.addby = req.body.addby;
       evnt.nim = req.body.nim;
     
        Event.create(evnt,function(err){
            if (!err){
              res.json({success:true, message:'Sukses Di Simpan!'});
            }else{
              res.json({success:false, message:'Gagal Cek Kembali!'});
            }
         });
    }

});

//GET ALL EVENT *EVENT
eventalm.get('/getall', function(req, res){
  Event.find({}, function(err, evnt){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, evnt:evnt});
    }
  })
});

//GET ALL EVENT WHERE NIM IN USE *EVENT
eventalm.get('/getallevntalm/:nim', function(req, res){
  var getOne = req.params.nim;
  Event.find({ nim:getOne}, function(err, evnt){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, evnt:evnt});
    }
  })
});

//GET FOR EVENT ID *EVENT
eventalm.get('/getevent/:id', function(req, res){
  var getOne = req.params.id;
  Event.findOne({ _id:getOne},function(err,evnt){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, evnt:evnt});
    }
  }); 
});

//GET ONE AND DEL EVENT *EVENT
eventalm.delete('/delloneevent/:id', function(req, res){
  var getOne = req.params.id;
  Event.findOneAndRemove({ _id: getOne}, function(err, evnt){
    if (err) throw err;
    if (!evnt){
      res.json({ success: false, message:'Gagal Event Tidak Ditemukan'});
      return evnt
    }else{
      res.json({ success:true, evnt:evnt });
      return evnt   
    }
  });  
});

// PUT ONE AND UPDATE EVENT *EVENT
eventalm.put('/updtevent', function(req, res){
    var getOne = req.body.id_group;
    Event.findOne({ _id: getOne}, function(err, edtevent){
        if (!edtevent){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return edtevent;        
        }else{
            
                          edtevent.judul = req.body.judul;
                          edtevent.urlslug = req.body.urlslug;
                          edtevent.imgs = req.body.imgs;
                          edtevent.postings = req.body.postings;
                          edtevent.tag = req.body.tag;
                          edtevent.save(function(err){
                            if (!err){
                                  res.json({success:true, message:'Sukses Di Edit!'});
                              return edtevent;                             
                            }else{
                              res.json({success:false, message:'Gagal Cek Kembali!'});
                            }
                          });
             }
    });
});


eventalm.get('/getallop', function(req, res){
  Event.find({ $or: [ { addby:'Superadmin' }, { addby: 'Admin' } ] }).exec(function(err, events){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, events:events});
    }
  })
});


eventalm.get('/getallnotop', function(req, res){
  Event.find({ $and: [ { addby: {'$ne':'Superadmin'} }, { addby: {'$ne':'Admin'} } ] }).exec(function(err, events){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, events:events});
    }
  })
});




return eventalm;
};