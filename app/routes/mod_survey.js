var Curvey        = require('../models/survey/SurveyAlm.js');
var fs          = require('fs');

module.exports = function(surveyalm) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //
//SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //SURVEY GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE SURVEY *SURVEY
surveyalm.post('/create', function(req, res, err){
  // fs.write('survey.json',req.body)
     console.log(req.body)
     Curvey.create(req.body, function(err,cek){
        if (err){
            res.json({ success: false, message: 'Data Tidak Ditemukan'});
            return cek;
        }else{
            res.json({ success:true});
            return cek;
        }
     })
});

//GET ALL SURVEY *SURVEY
surveyalm.get('/getall', function(req, res){
  Curvey.find({}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ID SURVEY *SURVEY
surveyalm.get('/getids/:id', function(req, res){
  Curvey.findOne({_id:req.params.id}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ID SURVEY *SURVEY ARRAY
surveyalm.post('/getbyarr', function(req, res){
  if(req.body.length <= 0){
    // console.log(err)
    res.json({ success: false, message: 'Data Tidak Ditemukan'});
  }else{
    var b = []
    for (var i = 0; i < req.body.length; i++){
      var a = req.body[i].id_survey;
      b.push(a)
      console.log('dalam for',b)
    }

     
  Curvey.find({_id:{'$nin':b}}).exec(function(err, surveys){
    if (err){
      console.log(err)
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      console.log(surveys)
      res.json({ success:true, surveys:surveys});
    }
  })
  }
});

// PUT ONE AND UPDATE Curvey *Curvey
surveyalm.put('/updtsurvey', function(req, res){
    var getOne = req.body.id_survey;
    Curvey.findOne({ _id: getOne}, function(err, edtsurvey){
        if (!edtsurvey){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return edtsurvey;        
        }else{
            console.log('from req',req.body.soal)
            console.log('from db',edtsurvey.soal)
            
            edtsurvey.soal = req.body.soal,
            edtsurvey.nama = req.body.nama;
            edtsurvey.deskripsi = req.body.deskripsi;
            edtsurvey.nim = req.body.nim;
            edtsurvey.addby = req.body.addby;
            edtsurvey.options = req.body.options;
            edtsurvey.save(req.body,function(err){
              if (!err){

                  res.json({success:true, message:'Sukses Di Edit!'});
                  return edtsurvey;                             
              }else{
                  res.json({success:false, message:'Gagal Cek Kembali!'});
              }
            });
          return edtsurvey;
        }
    });
});

//GET ONE AND DEL SURVEY *SURVEY
surveyalm.delete('/dellonesrvy/:id', function(req, res){
  var getOne = req.params.id;
  Curvey.findOneAndRemove({ _id: getOne}, function(err, survey){
    if (err) throw err;
    if (!survey){
      res.json({ success: false, message:'Gagal Event Tidak Ditemukan'});
      return survey
    }else{
      res.json({ success:true, survey:survey });
      return survey   
    }
  });  
});


// //GET ALL EVENT WHERE NIM IN USE *EVENT
// surveyalm.get('/getallevntalm/:nim', function(req, res){
//   var getOne = req.params.nim;
//   Event.find({ nim:getOne}, function(err, evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   })
// });

// //GET FOR EVENT ID *EVENT
// surveyalm.get('/getevent/:id', function(req, res){
//   var getOne = req.params.id;
//   Event.findOne({ _id:getOne},function(err,evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   }); 
// });



// // PUT ONE AND UPDATE EVENT *EVENT
// surveyalm.put('/updtevent', function(req, res){
//     var getOne = req.body.id_group;
//     Event.findOne({ _id: getOne}, function(err, edtevent){
//         if (!edtevent){
//           res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
//           return edtevent;        
//         }else{
//           // console.log('data', req.files);
//           // console.log('data2', req.files.imgs);
//             if (req.files.imgs === undefined){
//                 edtevent.judul = req.body.judul;
//                   // edtevent.imgs = req.files.imgs['path'];
//                   edtevent.postings = req.body.postings;
//                   edtevent.tag = req.body.tag;
//                   edtevent.save(function(err){
//                     if (!err){
//                         res.json({success:true, message:'Sukses Di Edit!'});
//                         return edtevent;                             
//                     }else{
//                       res.json({success:false, message:'Gagal Cek Kembali!'});
//                     }
//                   });
//             }else if(req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
//                     fs.unlink(edtevent.imgs, function(error) {
//                       if (error) {
//                           console.log('Error!!');
//                       }else{    
//                           edtevent.judul = req.body.judul;
//                           edtevent.imgs = req.files.imgs['path'];
//                           edtevent.postings = req.body.postings;
//                           edtevent.tag = req.body.tag;
//                           edtevent.save(function(err){
//                             if (!err){
//                                   res.json({success:true, message:'Sukses Di Edit!'});
//                               return edtevent;                             
//                             }else{
//                               res.json({success:false, message:'Gagal Cek Kembali!'});
//                             }
//                           });
//                           return edtevent;
//                             console.log('SAVE,selesai di hapus!!');
//                       }
//                     });
//             }else{
//                 res.json({ success: false, message: 'Extensi Harus Png/Jpg/Jpeg'});
//                 fs.unlink(req.files.imgs['path'], function(error) {
//                   if (error) {
//                    console.log('Error!!', Error);
//                   }else{
//                     console.log('selesai di hapus!!');
//                   }
//                   });
//             return edtevent;
//           }
//           return edtevent;
//         }
//     });
// });


// surveyalm.get('/getallop', function(req, res){
//   Event.find({ $or: [ { addby:'Superadmin' }, { addby: 'Admin' } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });


// surveyalm.get('/getallnotop', function(req, res){
//   Event.find({ $and: [ { addby: {'$ne':'Superadmin'} }, { addby: {'$ne':'Admin'} } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });




return surveyalm;
};