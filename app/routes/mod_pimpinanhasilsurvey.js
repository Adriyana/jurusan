var HasilCurveyPmp = require('../models/pimpinansurvey/RealHasilSurveyPimpinan.js');
var fs             = require('fs');

module.exports = function(pimpinanhasilsrvy) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //
//HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE HASILSURVEY *HASILSURVEY
pimpinanhasilsrvy.post('/create', function(req, res, err){
  // fs.write('HASILSURVEY.json',req.body)
     console.log(req.body)
     HasilCurveyPmp.create(req.body, function(err,cek){
        if (err){
            console.log(err)
            res.json({ success: false, message: 'Data Tidak Ditemukan'});
            return cek;
        }else{
            res.json({ success:true});
            return cek;
        }
     })
});

//GET ALL HASILSURVEY *HASILSURVEY
pimpinanhasilsrvy.get('/getall', function(req, res){
  HasilCurveyPmp.find({}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ID HASILSURVEY *HASILSURVEY
pimpinanhasilsrvy.get('/gettokens/:tokens', function(req, res){
  HasilCurveyPmp.find({tokens:req.params.tokens},function(err,surveys){
    // console.log(surveys)
    // console.log(err)
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ID HASILSURVEY *HASILSURVEY
pimpinanhasilsrvy.get('/getallid/:id', function(req, res){
  HasilCurveyPmp.find({id_survey:req.params.id}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});


return pimpinanhasilsrvy;
};