var Group        = require('../models/group/GroupAlm.js');
var fs          = require('fs');

module.exports = function(groupalm) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //
//GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //GROUP GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE GROUP *GROUP
groupalm.post('/groupcreate', function(req, res, err){
  var group = new Group();
     group.nama = req.body.nama;
     group.deskripsi = req.body.deskripsi;
     group.tag = req.body.tag;
     group.imgs = req.files.imgs['path'];
     group.addby = req.body.addby;
     group.nim = req.body.nim;
     

    if (req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
      group.save(function(err){
          if (!err){
            res.json({success:true, message:'Sukses Di Simpan!'});
          return group;
          }else{
            res.json({success:false, message:'Gagal Cek Kembali!'});
            console.log('errornya', err);
          return group;
          }
       });
    }else{
      res.json({ success:false, message: 'Extensi Harus Png/Jpg/Jpeg'});
      fs.unlink(req.files.imgs['path'], function(error) {
        if (error) {
         console.log('Error!!', Error);
        return group;
        }else{
          console.log('selesai di hapus!!');
        return group;
        }
        });
    }
     
});

//GET ALL GROUP *GROUP
groupalm.get('/getall', function(req, res){
  Group.find({}, function(err, grooup){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, grooup:grooup});
    }
  })
});

groupalm.get('/getallop', function(req, res){
  Group.find({ $or: [ { addby:'SUPERADMIN' }, { addby: 'ADMIN' } ] }).exec(function(err, group){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, group:group});
    }
  })
});


groupalm.get('/getallnotop', function(req, res){
  Group.find({ $and: [ { addby: {'$ne':'SUPERADMIN'} }, { addby: {'$ne':'ADMIN'} } ] }).exec(function(err, group){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, group:group});
    }
  })
});


//GET ALL GROUP WHERE NIM IN USE *GROUP
groupalm.get('/getallgroupalm/:nim', function(req, res){
  var getOne = req.params.nim;
  Group.find({ nim:getOne}, function(err, grooup){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, grooup:grooup});
    }
  })
});

// PUT ONE AND UPDATE GROUP *GROUP
groupalm.put('/updtgrp', function(req, res){
    var getOne = req.body.id_group;
    Group.findOne({ _id: getOne}, function(err, edtgrp){
        if (!edtgrp){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});           
          return edtgrp;
        }else{
          if (req.files.imgs === undefined ){
              edtgrp.nama = req.body.nama;
              edtgrp.deskripsi = req.body.deskripsi;
              edtgrp.tag = req.body.tag;
             
              edtgrp.save(function(err){
                if (!err){
                      res.json({success:true, message:'Sukses Di Edit!'});
                }else{
                      res.json({success:false, message:'Gagal Cek Kembali!'});
                }
              });
            return edtgrp;
          }else if (req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
              fs.unlink(edtgrp.imgs, function(error) {
                if (error) {
                  console.log('Error!!');
                }else{
                  console.log('SAVE,selesai di hapus!!');
                  edtgrp.nama = req.body.nama;
                  edtgrp.imgs = req.files.imgs['path'];
                  edtgrp.deskripsi = req.body.deskripsi;
                  edtgrp.tag = req.body.tag;
                 
                  edtgrp.save(function(err){
                    if (!err){
                          res.json({success:true, message:'Sukses Di Edit!'});
                             
                    }else{
                          res.json({success:false, message:'Gagal Cek Kembali!'});
                    }
                  });
                }
              });
            return edtgrp;
          }else{
                res.json({ success: false, message: 'Extensi Harus Png/Jpg/Jpeg'});
                fs.unlink(req.files.imgs['path'], function(error) {
                  if (error) {
                   console.log('Error!!', Error);
                  }else{
                    console.log('selesai di hapus!!');
                  }
                });
              return edtgrp;
          }
        }
    });
});

//GET ONE AND DEL GROUP *GROUP
groupalm.delete('/dellonegrp/:id', function(req, res){
  var getOne = req.params.id;
  Group.findOneAndRemove({ _id: getOne}, function(err, groups){
    if (err) throw err;
    if (!groups){
      res.json({ success: false, message:'Gagal Groups Tidak Ditemukan'});
      return groups;
    }else{
      res.json({ success:true, groups:groups });
      fs.unlink(groups.imgs, function(error) {
                  if (error) {
                   console.log('Error!!', Error);
                  }else{
                    console.log('selesai di hapus!!');
                  }
      }); 
    return groups;
    }
  });  
});

//GET ALL ANGGOTA FOLOW GROUP SUBDOC *ANGGOTA
groupalm.get('/getallflow/:nim', function(req, res){
  var getOne = req.params.nim;
  // console.log(getOne)
  // var options = {anggota:{$elemMatch:{nim:{$gte: getOne}}}}
  Group.find({'anggota.nim':getOne, 'anggota.active':1}).select('nama tag nim urlslug imgs active postings').exec(function(err,grpflow){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, grpflow:grpflow});
    }
  }); 
});

//GET ALL ANGGOTA FOLOW GROUP SUBDOC *ANGGOTA
groupalm.get('/getallconfirmflow/:nim', function(req, res){
  var getOne = req.params.nim;
  console.log('CEK NOTIF CONFIRMATION',getOne)
  // var options = {anggota:{$elemMatch:{nim:{$gte: getOne}}}}
  Group.find({'anggota.nim':getOne, 'anggota.active':0, 'anggota.addby':true }).select('nama deskripsi tag nim imgs active anggota created_at').exec(function(err,grpflowcnfrm){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, grpflowcnfrm:grpflowcnfrm});
    }
  }); 
});

//GET ALL FOR ANGGOTA GROUP *ANGGOTA
groupalm.get('/getallagt/:id', function(req, res){
  var getOne = req.params.id;
  Group.findOne({ _id:getOne},function(err,groupagt){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, groupagt:groupagt});
    }
  }); 
});


// PUT ONE AND ADD AGT *ANGGOTA
groupalm.put('/addagtalm', function(req, res){
  var getOne = req.body.id_group;
    Group.findOne({ _id: getOne}, function(err, grp){
      if (grp){
          grp.anggota.push({nim:req.body.nim, nama:req.body.nama, addby:req.body.addby})
              grp.save(function(err){
                if (!err){

                  res.json({success:true, message:'Sukses!'});
                  return grp;
                }else{
                  res.json({success:false, message:'Gagal Add Cek Kembali!'});
                  return grp;
                }
          });
       return grp;
      }else{
        res.json({success:false, message:'Data Group Tidak Diketahui!'});
        return grp;        
      }
      return grp;
    });
});

// CEK ONE ANGGOTA *ANGGOTA
groupalm.post('/checkagt', function(req, res){
   var getOne = req.body.id_group;
   var options = {anggota: {$elemMatch: { nim: req.body.nim }}};
        Group.findOne({ _id: getOne, 'anggota.nim': req.body.nim }, options, function(err, dataagt){
          if (dataagt){
            // fs.appendFile('logsagt.txt',"&&" + dataagt.anggota[0].nim, function(err){
            //   if(err){
            //     next();
            //   }
            // });
            // try {  
            //     var datas = fs.readFileSync('logsagt.txt', 'utf8');
            //     console.log('datafs',datas);    
            // } catch(e) {
            //     console.log('Error:', e.stack);
            // }
            // console.log(dataagt)
            res.json({success:true, message:'Anda Sudah Menambahkan Sebelumnya'});
            return dataagt;
          }else{

            // console.log('tidak cocok')
            res.json({success:false, message:'Data Tidak Cocok'});
            return dataagt;  
          }
});
});

//CEK ONE AND DEL ANGGOTA *ANGGOTA
groupalm.post('/dellone', function(req, res){
  console.log(req.body.idagt)
  var getOne = req.body.id_group;
   var options = {$pull: {anggota: { _id: req.body.idagt }},};
        Group.findOneAndUpdate({ _id: getOne, 'anggota._id': req.body.idagt }, options, {new: true}, function(err, dataagt){
          if (dataagt){
            res.json({success:true, message:'Sukses Di Hapus'});
            // console.log('succ',dataagt)
            return dataagt;
          }else{
            res.json({success:false, message:'Gagal Menghapus'});
            // console.log('err',err)
            return dataagt;  
          }
});
});

//CEK ONE AND DEL ANGGOTA *ANGGOTA
groupalm.post('/dellonepost', function(req, res){
  console.log(req.body.idPost)
  var getOne = req.body.id_group;
   var options = {$pull: {postings: { _id: req.body.idPost }},};
        Group.findOneAndUpdate({ _id: getOne, 'postings._id': req.body.idPost }, options, {new: true}, function(err, dataPst){
          if (dataPst){
            res.json({success:true, message:'Sukses Di Hapus', dataPst:dataPst});
            // console.log('succ',dataPst)
            return dataPst;
          }else{
            res.json({success:false, message:'Gagal Menghapus'});
            
            console.log('err',err)
            return dataPst;  
          }
});
});

// PUT ONE AND KONFIRM AGT  *KONFIRM ANGGOTA
groupalm.put('/konfagt', function(req, res){
  var getOne = req.body.id_group;
  var options = {$set: {"anggota.$.active": req.body.active, "anggota.$.addby": true}};
  Group.findOneAndUpdate({ _id: getOne, 'anggota.nim': req.body.nims }, options,{new: true}, function(err, dataagt){
          if (dataagt){
            // console.log(dataagt)
            res.json({success:true, message:'Sukses Mengkonfirmasi'});
            return dataagt;
          }else{
            res.json({success:false, message:'Gagal Konfirmasi Anggota'});
            return dataagt;  
          }
  });
});

// PUT ONE AND ADD AGT Admin *ANGGOTA ADMIN
groupalm.put('/addadmalm', function(req, res){
    var getOne = req.body.id_group;
   var options = {$set: {"anggota.$.admin": req.body.admin}};
        Group.findOneAndUpdate({ _id: getOne, 'anggota.nim': req.body.nims }, options,{new: true}, function(err, dataagt){
          if (dataagt){
            // console.log(dataagt)
            res.json({success:true, message:'Sukses Menambah Admin'});
            return dataagt;
          }else{
            res.json({success:false, message:'Gagal Menambah Admin'});
            return dataagt;  
          }
});
});

// PUT ONE AND DEL AGT Admin *ANGGOTA ADMIN
groupalm.put('/deladmalm', function(req, res){
    var getOne = req.body.id_group;
   var options = {$set: {"anggota.$.admin": req.body.admin}};
        Group.findOneAndUpdate({ _id: getOne, 'anggota.nim': req.body.nims }, options,{new: true}, function(err, dataagt){
          if (dataagt){
            // console.log(dataagt)
            res.json({success:true, message:'Sukses Menghapus Admin'});
            return dataagt;
          }else{
            res.json({success:false, message:'Gagal Menghapus Admin'});
            return dataagt;  
          }
});
});

// PUT ONE AND ADD POST ANGGOTA *ANGGOTA POSTINGS
groupalm.put('/addpostagt', function(req, res){
  var getOne = req.body.id_group;
    Group.findOne({ _id: getOne}, function(err, grp){
      if (grp){
          grp.postings.push({nim:req.body.nim, nama:req.body.nama, post:req.body.postings, id_group:getOne})
              grp.save(function(err){
                if (!err){
                  // console.log('datsnya',dats);
                  res.json({success:true, message:'Sukses!'});
                  return grp;
                }else{
                  res.json({success:false, message:'Gagal Cek Kembali!'});
                  return grp;
                }
          });
       return grp;
      }else{
        res.json({success:false, message:'Data Group Tidak Diketahui!'});
        return grp;        
      }
      return grp;
    });
});

// NOTIF CONFIRM  *KONFIRM ANGGOTA
groupalm.put('/confrmyesagt', function(req, res){
  var getOne = req.body.id_group;
  var options = {$set: {"anggota.$.active": req.body.active, "anggota.$.addby": false}};
  Group.findOneAndUpdate({ _id: getOne, 'anggota.nim': req.body.nims }, options,{new: true}, function(err, dataagt){
          if (dataagt){
            // console.log(dataagt)
            res.json({success:true, message:'Sukses Mengkonfirmasi'});
            return dataagt;
          }else{
            res.json({success:false, message:'Gagal Konfirmasi Anggota'});
            return dataagt;  
          }
  });
});

groupalm.get('/notifgroupalm/:nim', function(req, res){
  var getOne = req.params.nim;
  console.log('getOnes',getOne)
// { $and: [ {anggota:{$elemMatch:{nim:{$gte: getOne}}}}, {'anggota.$.active':1} ] }
  Group.find({'anggota.nim':getOne}, {'anggota.active':1}).sort({'postings.created_at':-1}).select('postings nama imgs urlslug').exec(function(err, grooup){
    if (err){
      // console.log('gagal')
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
        // console.log('sukses',grooup)
        res.json({ success:true, grooup:grooup});
    }
  })
});

groupalm.get('/allsnotifgroupalm', function(req, res){
  // var getOne = req.params.nim;
  Group.find({},{new: true}).sort({'postings.$.created_at':-1}).select('postings nama imgs urlslug').exec(function(err, grooup){
    if (err){

      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      // Group.findOne({ _id:grooup[0]._id}).sort({'postings.created_at':-1}, {new: true}).exec(function(err, grooup1){

        // console.log(grooup)
        res.json({ success:true, grooup:grooup});
        // console.log(grooup[0]._id)
      // })
    }
  })
});

// groupalm.get('/notifgroupalm/:nim', function(req, res){
//   var getOne = req.params.nim;
//   Group.find({ nim:getOne},function(err, grooup){
//     if (err){
//       console.log('data tidak ditemukan')
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       console.log('sukses',grooup)
//       res.json({ success:true, grooup:grooup});
//     }
//   })
// });

return groupalm;
};