// Dependencies
// var alm = 'http://103.55.33.52:3000/api/alm';
var alm = 'http://103.55.33.52:3000/api/profilAlm';
var uril = 'http://alumni.if.uinsgd.ac.id/mod_cronalm/crcreate';
var uriscrap = 'http://alumni.if.uinsgd.ac.id/mod_berita/newscreate';
var uriscrapEduc = 'http://alumni.if.uinsgd.ac.id/mod_event/create';
var uriScholars = 'http://alumni.if.uinsgd.ac.id/mod_scholar/create';
var Xray = require("x-ray");
var CronJob = require('node-cron');
var request = require('request');
var fs = require("fs");

module.exports = function() { 

// THIS SCRAPE LOKER
  this.CronScrap = function a(){
    CronJob.schedule('0 */23 * * *', function () {
    //BATAS CRON KE XRAY;
    var x = new Xray({
    filters: {
              trim: function (value) {
                return value.replace(/\r|\t|\n|€/g, "");
              },
              reverse: function (value) {
                return typeof value === 'string' ? value.split('').reverse().join('') : value
              },
              slice: function (value, start , end) {
                return typeof value === 'string' ? value.slice(start, end) : value
              },
              replaceLineBreak: function (value) { 
                return value.replace(/\r|\t|\n|€/g, '<p>'); }
            }
    })

      var url = "http://www.jobs.id/lowongan-kerja"
      x(url, '.single-job-ads', [{
        judul: '.col-md-10 h3 .bold',
        urlslug:'h3 a@href | slice:37',
        urlimage:'img @src',
        perusahaan:'.col-md-10 p .bold',
        lokasi:'p .location',
        gajih:['p > span.semi-bold + .semi-bold','p > span.semi-bold + span.semi-bold + span.semi-bold'],
        detail: x('.bold @href', {
            deskripsi: '.job_desc | replaceLineBreak',
            prasyarat:'.job_req | replaceLineBreak',
            bidang:'h4 a',
            date_on:'.col-xs-6 .text-gray | trim | slice:44',
            date_off:'.col-xs-6 .text-right | trim | slice:56'
          })
        }]).paginate('.pagination > li.active + li > a@href').limit(3)
        (function(err, news){
             if(err === undefined || null){
              console.log('Kesalahan Scrap LOKER',err);
            }else{
              var obbject = JSON.stringify(news);       
              var optionsscrap = {url: uriscrap, method: 'POST',headers: {'content-type' : 'application/json'}, body:  obbject}
                request(optionsscrap, function(err, res, data) {  
                  // let json = JSON.parse(body);
                  if (err){
                    console.log('err from scrap factory',err);
                  }else{
                    console.log('Scrap Telah Dikirim Ke DB');
                  }
                  console.log('sukses');
                  fs.writeFile('result.json', obbject);  
                  return data;
                }); 
            };
        });
      });
    };


// THIS SCRAPE EDUCATION
  this.CronEducation = function CronEducations(){
    CronJob.schedule('0 */22 * * *', function () {
    //BATAS CRON KE XRAY;
    var xEduc = new Xray({
    filters: {
              trim: function (value) {
                return value.replace(/\r|\t|\n|€/g, "");
              },
              reverse: function (value) {
                return typeof value === 'string' ? value.split('').reverse().join('') : value
              },
              slice: function (value, start , end) {
                return typeof value === 'string' ? value.slice(start, end) : value
              },
              replaceLineBreak: function (value) { 
                return value.replace(/\r|\t|\n|€/g, '<p>'); }
            }
    })

      var urlEduc = "http://jadwalevent.web.id/"
      xEduc(urlEduc, 'article', [{
        judul: '.post-inner .post-title | trim',
        urlslug:'div a@href | slice:26',
        urlimage:'img @src',
        detail: xEduc('div a@href', {
            deskripsi: '.entry | replaceLineBreak'
          })
        }])
      .paginate('.pagination .right a@href').limit(3)(function(err, Educ){
        // fs.writeFile('resultEducation.json', JSON.stringify(Educ)); 
        // console.log('Scrap Education')
            if(err === undefined || null){
              console.log('LOG Educ Err',err);
            }else{
              var EducOb = JSON.stringify(Educ);       
              var optionsEduc = {url: uriscrapEduc, method: 'POST',headers: {'content-type' : 'application/json'}, body:  EducOb}
                request(optionsEduc, function(err, res, data) {  
                  if (err){
                    console.log('err from scrap Education',err);
                    return false;
                  }else{
                    console.log('ScrapEducation Telah Dikirim Ke DB');
                    return true;
                  }
                  console.log('EducationSukses');
                }); 
            };
        });
      });
    };


// THIS SCRAPE SCHOLARSHIP
  this.CronScholar = function CronScholars(){
    CronJob.schedule('0 */21 * * *', function () {
    //BATAS CRON KE XRAY;
    var xScholarships = new Xray({
    filters: {
              trim: function (value) {
                return value.replace(/\r|\t|\n|€/g, ", ");
              },
              reverse: function (value) {
                return typeof value === 'string' ? value.split('').reverse().join('') : value
              },
              slice: function (value, start , end) {
                return typeof value === 'string' ? value.slice(start, end) : value
              },
              replaceLineBreak: function (value) { 
                return value.replace(/\r|\t|\n|€/g, '<p>'); }
            }
    })
      // var SubURL = ['europe','asia']
      // var ResArr = [];
      var urlScholarships = "https://beasiswaindo.com/scholarships/"
      // for (var i in SubURL){
      xScholarships(urlScholarships, '.site-main>article', [{
        judul: 'header>h2>a',
        // urlslug:'header>h2>a@href | slice:25',
        urlimg:'div>p>img@data-src',
        detail: xScholarships('header>h2 a@href', {
            deskripsi: 'div.entry-content | replaceLineBreak',
          })
        }]).paginate('nav>div>span.page-numbers.current+a@href').limit(5)(function(err, Scholarships){
        // ResArr.push(Scholarships)
        
            if(err === undefined || null){
              console.log('ErrFrom ScholarScrap',err);
            }else{
              console.log('Scrap Scholar')
               fs.writeFile('resultScholar.json', JSON.stringify(Scholarships)); 
              var dataArr = []; 
              // var obbSchol; 
              Scholarships.map(function(e) { 
                if ((e.judul !== undefined) || (e.detail['deskripsi'] !== undefined)){
                   dataArr.push({"judul":e.judul, "urlimg":e.urlimg,"deskripsi":e.detail.deskripsi})   
                } 
              })
              fs.writeFile('resultScholar1.json', JSON.stringify(dataArr)); 
              // for (var i in Scholarships){
              //   if ((Scholarships[i].judul !== undefined || null)||(Scholarships[i].detail['deskripsi'] !== undefined || null)){
              //       dataArr.push({"judul":Scholarships[i].judul, "urlimg":Scholarships[i].urlimg,"deskripsi":Scholarships[i].detail.deskripsi})
              //     }
              // }
              //   fs.writeFile('resultScholar1.json', JSON.stringify(dataArr));  
              var optionsScholar = {url: uriScholars, method: 'POST',headers: {'content-type' : 'application/json'}, body:  JSON.stringify(dataArr)}
                      request(optionsScholar, function(err, res, data) {  
                       
                        if (err){
                          console.log('err from scrapScholar',err);
                          
                        }else{
                          console.log('Scrap Telah Dikirim Ke DB');
                          return true;
                        }
                        console.log('suksesScholarScrp');
                      });
            }
        });
       });
    };

// THIS CRON ALUMNI
    this.CronAlumni = function cronAlm(){
      CronJob.schedule('00 30 11 * * 4-5', function () {
        var options = {url: alm,method: 'GET',headers: {'content-type' : 'application/json'}}// 'Accept-Charset': 'utf-8'};        
        
        request(options, function(err, res, alumni, req) {  
              // let var json = JSON.parse(alumni);
          console.log(alumni)
          if (alumni == undefined || null){
              console.log('Data_Hasil_Cron,Data Alumni Sukses Di Cron, Tapi Kosong');
              fs.writeFile('results.cralm.err.json', alumni);  
              return alumni;
          }else{
            // if (alumni.length > 50){
              var options1 = {url: uril,method: 'POST',headers: {'content-type' : 'application/json'}, body:  alumni}
              request(options1, function(err, data){
                if (err){
                  console.log('test ambil err',err);
                  fs.writeFile('results.cralm1.err.json', err);  
                  return alumni;
                }else{
                  fs.writeFile('results.cralm.success.json', alumni);  
                  console.log('Data_Hasil_Cron, Data Alumni Sukses Diproses');
                  return alumni;
                }
                return alumni;
              });
            // }else{
            //   console.log('Data_Hasil_Cron :', alumni);
            //   // console.log('Data_Hasil_Cron1', alm);
            //   // console.log('Data_Hasil_Cron2', res);
            //   // console.log('Data_Hasil_Cron3', err);
            //   fs.writeFile('results.cralm.true.json', alumni);  
            //   return alumni;
            // }
          }
        });    
      });
    };
    
};