var AlumniHasilCurvey        = require('../models/survey1/RealHasilSurveyAlm.js');
var fs          = require('fs');

module.exports = function(alumnihasilsrvy) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //
//HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE HASILSURVEY *HASILSURVEY
alumnihasilsrvy.post('/create', function(req, res, err){
  // fs.write('HASILSURVEY.json',req.body)
     console.log(req.body)
      AlumniHasilCurvey.find({id_survey:req.body.id_survey}).then(function(result){
        var dataI = result.length - 1
        if (result.length > 1){
          console.log('data banyak')
          var b = [];
              result[dataI].hasilawal.forEach(function(o, i) {
                b.push(parseInt(result[dataI].hasilawal[i]) + parseInt(req.body.jawaban[i].hasil))
              });    
          // console.log('b',b)
          AlumniHasilCurvey.update({ id_survey: { $in: req.body.id_survey } },
            { $set: { hasilawal: b } },{ multi: true },function(err){
            if (!err){
                var Hcurvey = new AlumniHasilCurvey();
                   Hcurvey.jawaban = req.body.jawaban;
                   Hcurvey.id_survey = req.body.id_survey;
                   Hcurvey.hasilawal = b;
                   Hcurvey.addby = req.body.addby;
                   Hcurvey.nim = req.body.nim;
                   Hcurvey.save(function(err){
                      if (err){
                        res.json({success:true, message:'Gagal Silahkan Cek Kembali'});
                      }else{
                        res.json({success:true, message:'Sukses Di Survey!'});
                      }
                   })
            }else{
                res.json({success:false, message:'Gagal Cek Kembali!'});
            }
          })
        }else if(result.length === 1){
          var a = [];
          result[dataI].jawaban.forEach(function(o, i) {
            a.push(parseInt(result[dataI].jawaban[i][0].hasil) + parseInt(req.body.jawaban[i].hasil))
               
          });
          result[dataI].hasilawal = a;
          result[dataI].save(function(err){
            if (!err){
                var Hcurvey = new AlumniHasilCurvey();
                   Hcurvey.jawaban = req.body.jawaban;
                   Hcurvey.id_survey = req.body.id_survey;
                   Hcurvey.hasilawal = a;
                   Hcurvey.addby = req.body.addby;
                   Hcurvey.nim = req.body.nim;
                   Hcurvey.save(function(err){
                      if (err){
                        res.json({success:true, message:'Gagal Silahkan Cek Kembali'});
                      }else{
                        res.json({success:true, message:'Sukses Di Survey!'});
                      }
                   })
            }else{
              // console.log('SALAH')
                var Hcurvey = new AlumniHasilCurvey();
                   Hcurvey.jawaban = req.body.jawaban;
                   Hcurvey.id_survey = req.body.id_survey;
                   Hcurvey.hasilawal = a;
                   Hcurvey.addby = req.body.addby;
                   Hcurvey.nim = req.body.nim;
                   Hcurvey.save(function(err){
                      if (err){
                        res.json({success:true, message:'Gagal Silahkan Cek Kembali'});
                      }else{
                        res.json({success:true, message:'Sukses Di Survey!'});
                      }
                   })
            }
          })                  
        }else{

              var Hcurvey1 = new AlumniHasilCurvey();
                   Hcurvey1.jawaban = req.body.jawaban;
                   Hcurvey1.id_survey = req.body.id_survey;
                   Hcurvey1.hasilawal = req.body.jawaban;
                   Hcurvey1.addby = req.body.addby;
                   Hcurvey1.nim = req.body.nim;
                   Hcurvey1.save(function(err){
                      if (err){
                        res.json({success:true, message:'Gagal Silahkan Cek Kembali'});
                      }else{
                        res.json({success:true, message:'Sukses Di Survey!'});
                      }
                   })
        }
        
      })
});

//GET ALL HASILSURVEY *HASILSURVEY
alumnihasilsrvy.post('/createdinamissurvey', function(req, res){
  AlumniHasilCurvey.create(req.body, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Gagal Survey'});
      console.log('err',err)
    }else{
      res.json({ success:true, message: 'Sukses Survey'});
    }
  })
});

//GET ALL HASILSURVEY *HASILSURVEY
alumnihasilsrvy.get('/getall', function(req, res){
  AlumniHasilCurvey.find({}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

alumnihasilsrvy.get('/getalldistinc', function(req, res){
   AlumniHasilCurvey.find().distinct("nim",function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
      // console.log(surveys)
    }
  })
});



//GET ID HASILSURVEY *HASILSURVEY
alumnihasilsrvy.get('/getnims/:nim', function(req, res){
  AlumniHasilCurvey.find({nim:req.params.nim}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ID HASILSURVEY *HASILSURVEY
alumnihasilsrvy.get('/getallid/:id', function(req, res){
  AlumniHasilCurvey.find({id_survey:req.params.id}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ONE ID HASILSURVEY *HASILSURVEY
alumnihasilsrvy.get('/getid/:id', function(req, res){
  AlumniHasilCurvey.findOne({_id:req.params.id}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

// //GET ALL EVENT WHERE NIM IN USE *EVENT
// alumnihasilsrvy.get('/getallevntalm/:nim', function(req, res){
//   var getOne = req.params.nim;
//   Event.find({ nim:getOne}, function(err, evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   })
// });

// //GET FOR EVENT ID *EVENT
// alumnihasilsrvy.get('/getevent/:id', function(req, res){
//   var getOne = req.params.id;
//   Event.findOne({ _id:getOne},function(err,evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   }); 
// });

// //GET ONE AND DEL EVENT *EVENT
// alumnihasilsrvy.delete('/delloneevent/:id', function(req, res){
//   var getOne = req.params.id;
//   Event.findOneAndRemove({ _id: getOne}, function(err, evnt){
//     if (err) throw err;
//     if (!evnt){
//       res.json({ success: false, message:'Gagal Event Tidak Ditemukan'});
//       return evnt
//     }else{
//       res.json({ success:true, evnt:evnt });
//       fs.unlink(evnt.imgs, function(error) {
//                   if (error) {
//                    console.log('Error!!', Error);
//                   }else{
//                     console.log('selesai di hapus!!');
//                   }
//       }); 
//       return evnt   
//     }
//   });  
// });

// // PUT ONE AND UPDATE EVENT *EVENT
// alumnihasilsrvy.put('/updtevent', function(req, res){
//     var getOne = req.body.id_group;
//     Event.findOne({ _id: getOne}, function(err, edtevent){
//         if (!edtevent){
//           res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
//           return edtevent;        
//         }else{
//           // console.log('data', req.files);
//           // console.log('data2', req.files.imgs);
//             if (req.files.imgs === undefined){
//                 edtevent.judul = req.body.judul;
//                   // edtevent.imgs = req.files.imgs['path'];
//                   edtevent.postings = req.body.postings;
//                   edtevent.tag = req.body.tag;
//                   edtevent.save(function(err){
//                     if (!err){
//                         res.json({success:true, message:'Sukses Di Edit!'});
//                         return edtevent;                             
//                     }else{
//                       res.json({success:false, message:'Gagal Cek Kembali!'});
//                     }
//                   });
//             }else if(req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
//                     fs.unlink(edtevent.imgs, function(error) {
//                       if (error) {
//                           console.log('Error!!');
//                       }else{    
//                           edtevent.judul = req.body.judul;
//                           edtevent.imgs = req.files.imgs['path'];
//                           edtevent.postings = req.body.postings;
//                           edtevent.tag = req.body.tag;
//                           edtevent.save(function(err){
//                             if (!err){
//                                   res.json({success:true, message:'Sukses Di Edit!'});
//                               return edtevent;                             
//                             }else{
//                               res.json({success:false, message:'Gagal Cek Kembali!'});
//                             }
//                           });
//                           return edtevent;
//                             console.log('SAVE,selesai di hapus!!');
//                       }
//                     });
//             }else{
//                 res.json({ success: false, message: 'Extensi Harus Png/Jpg/Jpeg'});
//                 fs.unlink(req.files.imgs['path'], function(error) {
//                   if (error) {
//                    console.log('Error!!', Error);
//                   }else{
//                     console.log('selesai di hapus!!');
//                   }
//                   });
//             return edtevent;
//           }
//           return edtevent;
//         }
//     });
// });


// alumnihasilsrvy.get('/getallop', function(req, res){
//   Event.find({ $or: [ { addby:'Superadmin' }, { addby: 'Admin' } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });


// alumnihasilsrvy.get('/getallnotop', function(req, res){
//   Event.find({ $and: [ { addby: {'$ne':'Superadmin'} }, { addby: {'$ne':'Admin'} } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });




return alumnihasilsrvy;
};