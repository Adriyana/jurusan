var TdyNews        = require('../models/global/Globals.Tdynews.js');
var fs          = require('fs');

module.exports = function(tdynews) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //
//TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //TDYNEWS GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE
tdynews.post('/newscreate', function(req, res, err){
  var tdynews = new TdyNews();
    tdynews.judul = req.body.judul;
    tdynews.imgs = req.files.imgs['path'];
    tdynews.deskripsi = req.body.deskripsi;
    tdynews.bidang = req.body.bidang;
    tdynews.addby = req.body.addby;
    tdynews.nim = req.body.nim;

    if (req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
      tdynews.save(function(err){
          if (!err){
            res.json({success:true, message:'Sukses Di Simpan!'});
          }else{
            res.json({success:false, message:err});
          }
       });
    }else{
      res.json({ success:false, message: 'Extensi Harus Png/Jpg/Jpeg'});
      fs.unlink(req.files.imgs['path'], function(error) {
        if (error) {
         console.log('Error!!', Error);
        }else{
          console.log('selesai di hapus!!');
        }
        });
    }
});
//GET ALL
tdynews.get('/getallnews', function(req, res){
  TdyNews.find({}, function(err, news){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, news:news});
    }
  })
});
//GET ALL WHERE
tdynews.get('/getallnewsalm/:nim', function(req, res){
  var getOne = req.params.nim;
  TdyNews.find({ nim:getOne, addby:'Alumni'}, function(err, news){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, news:news});
    }
  })
});
//GET ONE
tdynews.get('/gettone/:id', function(req,res){
  var getOne = req.params.id;
  TdyNews.findOne({ _id: getOne}, function(err, tdynews){
    if (err)throw err;
    if (!tdynews){
      res.json({ success: false, message:'tdynews Tidak Ditemukan'});
    }else{
      res.json({ success:true, tdynews:tdynews });     
    }
  });
});

// PUT ONE
tdynews.put('/putonee', function(req, res){
  var getOne = req.body._id;
  // console.log(getOne);
  // console.log(req.files);
  // console.log(req.body.judul);
    TdyNews.findOne({ _id: getOne}, function(err, news){
    //   // if (err) throw err;
        if (!news){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});           
          // console.log(req.body);
          // console.log(news);
        }else{
          if (req.files.imgs === undefined){
              news.judul = req.body.judul;
              news.deskripsi = req.body.deskripsi;
              news.bidang = req.body.bidang;
              news.save(function(err){
                  if (!err){
                        res.json({success:true, message:'Sukses Di Edit!'});
                  }else{
                        res.json({success:false, message:'Gagal Cek Kembali!'});
                  }
                });
          }else if (req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
                fs.unlink(news.imgs, function(error) {
                  if (error) {
                   console.log('Error!!');
                  }else{
                    news.judul = req.body.judul;
                    news.imgs = req.files.imgs['path'];
                    news.deskripsi = req.body.deskripsi;
                    news.bidang = req.body.bidang;
                    news.save(function(err){
                      if (!err){
                            res.json({success:true, message:'Sukses Di Edit!'});
                      }else{
                            res.json({success:false, message:'Gagal Cek Kembali!'});
                      }
                    });
                  }
                });
            }else{
                res.json({ success: false, message: 'Extensi Harus Png/Jpg/Jpeg'});
                fs.unlink(req.files.imgs['path'], function(error) {
                  if (error) {
                   console.log('Error!!', Error);
                  }else{
                    console.log('selesai di hapus!!');
                  }
                  });
              }
        }
    });
});

//GET ONE AND DEL
tdynews.delete('/dellone/:id', function(req, res){
  var getOne = req.params.id;
  TdyNews.findOneAndRemove({ _id: getOne}, function(err, tdynews){
    if (err) throw err;
    if (!tdynews){
      res.json({ success: false, message:'Gagal tdynews Tidak Ditemukan'});
    }else{
      fs.unlink(tdynews.imgs, function(error) {
        if (error) {
          console.log('Error!!', Error);
        }else{
          console.log('selesai di hapus!!');
          res.json({ success:true, tdynews:tdynews });     
        }
      });
      
    }
  });  
});

//GET ALL MOM *MOM
tdynews.get('/getallop', function(req, res){
  TdyNews.find({ $or: [ { addby:'SUPERADMIN' }, { addby: 'ADMIN' } ] }).exec(function(err, tdy){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, tdy:tdy});
    }
  })
});


tdynews.get('/getallnotop', function(req, res){
  TdyNews.find({ $and: [ { addby: {'$ne':'SUPERADMIN'} }, { addby: {'$ne':'ADMIN'} } ] }).exec(function(err, tdy){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, tdy:tdy});
    }
  })
});

return tdynews;
};