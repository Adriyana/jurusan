var Mom        = require('../models/mom/MomAlm.js');
var fs          = require('fs');

module.exports = function(momalm) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Mom GLOBAL MODEL //Mom GLOBAL MODEL //Mom GLOBAL MODEL //Mom GLOBAL MODEL //Mom GLOBAL MODEL //
//Mom GLOBAL MODEL //Mom GLOBAL MODEL //Mom GLOBAL MODEL //Mom GLOBAL MODEL //Mom GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE MOM *MOM
momalm.post('/create', function(req, res, err){
  var mom = new Mom();
     mom.judul = req.body.judul;
     mom.postings = req.body.postings;
     mom.tag = req.body.tag;
     mom.addby = req.body.addby;
     mom.nim = req.body.nim;
     
     mom.save(function(err){
          if (!err){
            res.json({success:true, message:'Sukses Di Simpan!'});
          return mom;
          }else{
            res.json({success:false, message:'Gagal Cek Kembali!'});
            console.log('errornya', err);
          return mom;
          }
       });     
});

//GET ALL MOM *MOM
momalm.get('/getall', function(req, res){
  Mom.find({}, function(err, mom){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, mom:mom});
    }
  })
});

//GET ALL MOM WHERE NIM IN USE *MOM
momalm.get('/getallmomtalm/:nim', function(req, res){
  var getOne = req.params.nim;
  Mom.find({ nim:getOne}, function(err, mom){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, mom:mom});
    }
  })
});

//GET FOR MOM ID *MOM
momalm.get('/getmom/:id', function(req, res){
  var getOne = req.params.id;
  Mom.findOne({ _id:getOne},function(err,mom){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    return mom;
    }else{
      res.json({ success:true, mom:mom});
      return mom;
    }
  }); 
});

//GET ONE AND DEL MOM *MOM
momalm.delete('/dellonemom/:id', function(req, res){
  var getOne = req.params.id;
  Mom.findOneAndRemove({ _id: getOne}, function(err, mom){
    if (err) throw err;
    if (!mom){
      res.json({ success: false, message:'Gagal MOM Tidak Ditemukan'});
      return mom
    }else{
      res.json({ success:true, mom:mom });
      return mom   
    }
  });  
});

// PUT ONE AND UPDATE MOM *MOM
momalm.put('/updtmom', function(req, res){
    var getOne = req.body.id_mom;
    Mom.findOne({ _id: getOne}, function(err, edtmom){
        if (!edtmom){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return edtmom;        
        }else{
            edtmom.judul = req.body.judul;
            edtmom.postings = req.body.postings;
            edtmom.tag = req.body.tag;
            edtmom.save(function(err){
              if (!err){
                  res.json({success:true, message:'Sukses Di Edit!'});
                  return edtmom;                             
              }else{
                  res.json({success:false, message:'Gagal Cek Kembali!'});
              }
           });
          return edtmom;
        }
    });
});

/////////////////////////////////////////////////////////////////////////////////////////////////
//ADMIN
/////////////////////////////////////////////////////////////////////////////////////////////////

//GET ALL MOM *MOM
momalm.get('/getallop', function(req, res){
  Mom.find({ $or: [ { addby:'SUPERADMIN' }, { addby: 'ADMIN' } ] }).exec(function(err, mom){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, mom:mom});
    }
  })
});


momalm.get('/getallnotop', function(req, res){
  Mom.find({ $and: [ { addby: {'$ne':'SUPERADMIN'} }, { addby: {'$ne':'ADMIN'} } ] }).exec(function(err, mom){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, mom:mom});
    }
  })
});



return momalm;
};