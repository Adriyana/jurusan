var Scholars        = require('../models/scholar/Scholars.js');
var fs          = require('fs');

module.exports = function(scholar) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //
//Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //Scholars GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE Scholars *Scholars
scholar.post('/create', function(req, res, err){
 if (req.body.addby === undefined){
  console.log('test')
  var bulk = Scholars.collection.initializeOrderedBulkOp(),
  counter = 0;
  req.body.forEach(function(doc) {
    bulk.find({judul:doc.judul}).upsert().update(
    {
     $setOnInsert: { 
      
      addby: 'Admin'
    },
                   // $currentDate: { created_at: true },
                   $set: { 
                    judul: doc.judul, 
                    urlslug:doc.judul.replace(/\s+/g, '-'),
                    urlimg: doc.urlimg, 
                    postings:doc.deskripsi
                        // danaawal: doc.detail['dana'],
                        // keterangan: doc.detail['keterangan']
                      }
                    }
                    )

    counter++;
    if (counter % 500 == 0) {
      bulk.execute(function(err, rslt) {
       if (err){
        console.log('err from counter', err)
        res.json({success:false, message:'Err From Counter!'});
      }else{
        console.log('success from counter',rslt)
        res.json({success:true, message:'Sukses From Counter!'});
        bulk = Scholars.collection.initializeOrderedBulkOp();
        counter = 0;
      }
    });
    }
  });

        // Catch any docs in the queue under or over the 500's
        if (counter > 0) {
          bulk.execute(function(err,result) {
           if (err){
            console.log('errscholar',err)
                // res.json({success:false, message:'Scrap! Gagal Di simpan!'});
              }else{
                // res.json({success:true, message:'Sukses Di Simpan!'});
                console.log('ssucscholar',result)
              }
            });
        }

      }else{
       var scholarship = {};
       scholarship.judul = req.body.judul;
       scholarship.postings = req.body.postings;
       scholarship.urlslug = req.body.urlslug;
       scholarship.urlimg = req.body.imgs;
       scholarship.addby = req.body.addby;
       scholarship.nim = req.body.nim;

       Scholars.create(scholarship,function(err){
        if (!err){
          res.json({success:true, message:'Sukses Di Simpan!'});
        }else{
          res.json({success:false, message:'Gagal Cek Kembali!'});
        }
      });
     }

   });

//GET ALL Scholars *Scholars
scholar.get('/getall', function(req, res){
  Scholars.find({}, function(err, scholarship){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, scholarship:scholarship});
    }
  })
});

//GET ONE AND DEL Scholars *Scholars
scholar.delete('/dellone/:id', function(req, res){
  var getOne = req.params.id;
  Scholars.findOneAndRemove({ _id: getOne}, function(err, scholarship){
    if (err){console.log('err')};
    if (!scholarship){
      res.json({ success: false, message:'Gagal Scholars Tidak Ditemukan'});
      return scholarship
    }else{
      res.json({ success:true, scholarship:scholarship });
      return scholarship   
    }
  });  
});


//GET FOR Scholars ID *Scholars
scholar.get('/getone/:id', function(req, res){
  var getOne = req.params.id;
  Scholars.findOne({ _id:getOne},function(err,scholarship){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, scholarship:scholarship});
    }
  }); 
});


// PUT ONE AND UPDATE Scholars *Scholars
scholar.put('/updtscholar', function(req, res){
  var getOne = req.body.id_group;
  Scholars.findOne({ _id: getOne}, function(err, edtscholar){
    if (!edtscholar){
      res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
      return edtscholar;        
    }else{
      edtscholar.judul = req.body.judul;
      edtscholar.urlimg = req.body.imgs;
      edtscholar.postings = req.body.postings;
      edtscholar.urlslug = req.body.urlslug;
      edtscholar.save(function(err){
        if (!err){
          res.json({success:true, message:'Sukses Di Edit!'});
          return edtscholar;                             
        }else{
          res.json({success:false, message:'Gagal Cek Kembali!'});
          return edtscholar;
        }
      });
    }
  });
});

return scholar;
};