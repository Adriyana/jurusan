var HasilCurveyPmp = require('../models/surveypemimpin/HasilSurveyPemimpin.js');
var fs             = require('fs');

module.exports = function(hasilsurveypemimpin) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //
//HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //HASILSURVEY GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE HASILSURVEY *HASILSURVEY
hasilsurveypemimpin.post('/create', function(req, res, err){
  // fs.write('HASILSURVEY.json',req.body)
     console.log(req.body)
     HasilCurveyPmp.create(req.body, function(err,cek){
        if (err){
            console.log(err)
            res.json({ success: false, message: 'Data Tidak Ditemukan'});
            return cek;
        }else{
            res.json({ success:true});
            return cek;
        }
     })
});

//GET ALL HASILSURVEY *HASILSURVEY
hasilsurveypemimpin.get('/getall', function(req, res){
  HasilCurveyPmp.find({}, function(err, surveys){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

//GET ID HASILSURVEY *HASILSURVEY
hasilsurveypemimpin.get('/gettokens/:tokens', function(req, res){
  HasilCurveyPmp.find({tokens:req.params.tokens}).sort({'id_survey':-1}).select("hp results email tokens id_survey jawaban jabatan addby created_at").exec(function(err,surveys){
    // console.log(surveys)
    // console.log(err)
    if ((surveys.length) === 0){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, surveys:surveys});
    }
  })
});

// //GET ALL EVENT WHERE NIM IN USE *EVENT
// hasilsurveypemimpin.get('/getallevntalm/:nim', function(req, res){
//   var getOne = req.params.nim;
//   Event.find({ nim:getOne}, function(err, evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   })
// });

// //GET FOR EVENT ID *EVENT
// hasilsurveypemimpin.get('/getevent/:id', function(req, res){
//   var getOne = req.params.id;
//   Event.findOne({ _id:getOne},function(err,evnt){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, evnt:evnt});
//     }
//   }); 
// });

// //GET ONE AND DEL EVENT *EVENT
// hasilsurveypemimpin.delete('/delloneevent/:id', function(req, res){
//   var getOne = req.params.id;
//   Event.findOneAndRemove({ _id: getOne}, function(err, evnt){
//     if (err) throw err;
//     if (!evnt){
//       res.json({ success: false, message:'Gagal Event Tidak Ditemukan'});
//       return evnt
//     }else{
//       res.json({ success:true, evnt:evnt });
//       fs.unlink(evnt.imgs, function(error) {
//                   if (error) {
//                    console.log('Error!!', Error);
//                   }else{
//                     console.log('selesai di hapus!!');
//                   }
//       }); 
//       return evnt   
//     }
//   });  
// });

// // PUT ONE AND UPDATE EVENT *EVENT
// hasilsurveypemimpin.put('/updtevent', function(req, res){
//     var getOne = req.body.id_group;
//     Event.findOne({ _id: getOne}, function(err, edtevent){
//         if (!edtevent){
//           res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
//           return edtevent;        
//         }else{
//           // console.log('data', req.files);
//           // console.log('data2', req.files.imgs);
//             if (req.files.imgs === undefined){
//                 edtevent.judul = req.body.judul;
//                   // edtevent.imgs = req.files.imgs['path'];
//                   edtevent.postings = req.body.postings;
//                   edtevent.tag = req.body.tag;
//                   edtevent.save(function(err){
//                     if (!err){
//                         res.json({success:true, message:'Sukses Di Edit!'});
//                         return edtevent;                             
//                     }else{
//                       res.json({success:false, message:'Gagal Cek Kembali!'});
//                     }
//                   });
//             }else if(req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
//                     fs.unlink(edtevent.imgs, function(error) {
//                       if (error) {
//                           console.log('Error!!');
//                       }else{    
//                           edtevent.judul = req.body.judul;
//                           edtevent.imgs = req.files.imgs['path'];
//                           edtevent.postings = req.body.postings;
//                           edtevent.tag = req.body.tag;
//                           edtevent.save(function(err){
//                             if (!err){
//                                   res.json({success:true, message:'Sukses Di Edit!'});
//                               return edtevent;                             
//                             }else{
//                               res.json({success:false, message:'Gagal Cek Kembali!'});
//                             }
//                           });
//                           return edtevent;
//                             console.log('SAVE,selesai di hapus!!');
//                       }
//                     });
//             }else{
//                 res.json({ success: false, message: 'Extensi Harus Png/Jpg/Jpeg'});
//                 fs.unlink(req.files.imgs['path'], function(error) {
//                   if (error) {
//                    console.log('Error!!', Error);
//                   }else{
//                     console.log('selesai di hapus!!');
//                   }
//                   });
//             return edtevent;
//           }
//           return edtevent;
//         }
//     });
// });


// hasilsurveypemimpin.get('/getallop', function(req, res){
//   Event.find({ $or: [ { addby:'Superadmin' }, { addby: 'Admin' } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });


// hasilsurveypemimpin.get('/getallnotop', function(req, res){
//   Event.find({ $and: [ { addby: {'$ne':'Superadmin'} }, { addby: {'$ne':'Admin'} } ] }).exec(function(err, events){
//     if (err){
//       res.json({ success: false, message: 'Data Tidak Ditemukan'});
//     }else{
//       res.json({ success:true, events:events});
//     }
//   })
// });




return hasilsurveypemimpin;
};