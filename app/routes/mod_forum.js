var Forum        = require('../models/forum/ForumAlm.js');
var fs          = require('fs');

module.exports = function(forumalm) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //
//FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //FORUM GLOBAL MODEL //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//CREATE FORUMS *FORUM
forumalm.post('/frmcreate', function(req, res, err){
  var forum = new Forum();
     forum.judul = req.body.judul;
     forum.postings = req.body.postings;
     forum.tag = req.body.tag;
     forum.imgs = req.files.imgs['path'];
     forum.addby = req.body.addby;
     forum.nim = req.body.nim;
     

    if (req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
      forum.save(function(err){
          if (!err){
            res.json({success:true, message:'Sukses Di Simpan!'});
          return forum;
          }else{
            res.json({success:false, message:'Gagal Cek Kembali!'});
            console.log('errornya', err);
          return forum;
          }
       });
    }else{
      res.json({ success:false, message: 'Extensi Harus Png/Jpg/Jpeg'});
      fs.unlink(req.files.imgs['path'], function(error) {
        if (error) {
         console.log('Error!!', Error);
        return forum;
        }else{
          console.log('selesai di hapus!!');
        return forum;
        }
        });
    }
     
});

//GET ALL FORUMS *FORUMS
forumalm.get('/getall', function(req, res){
  Forum.find({}, function(err, forum){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, forum:forum});
    }
  })
});

forumalm.get('/getallop', function(req, res){
  Forum.find({ $or: [ { addby:'SUPERADMIN' }, { addby: 'ADMIN' } ] }).exec(function(err, forums){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, forums:forums});
    }
  })
});


forumalm.get('/getallnotop', function(req, res){
  Forum.find({ $and: [ { addby: {'$ne':'SUPERADMIN'} }, { addby: {'$ne':'ADMIN'} } ] }).exec(function(err, forums){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, forums:forums});
    }
  })
});


//GET ALL FORUMS WHERE NIM IN USE *FORUMS
forumalm.get('/getallforumalm/:nim', function(req, res){
  var getOne = req.params.nim;
  Forum.find({ nim:getOne}, function(err, forum){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, forum:forum});
    }
  })
});

//GET FORUMS ID *FORUMS
forumalm.get('/getallpost/:id', function(req, res){
  var getOne = req.params.id;
  Forum.findOne({ _id:getOne},function(err,forums){
    if (err){
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      res.json({ success:true, forums:forums});
    }
  }); 
});


// PUT ONE AND ADD POST ANGGOTA *ANGGOTA POSTINGS
forumalm.put('/addpost', function(req, res){
  var getOne = req.body.id_group;
    Forum.findOne({ _id: getOne}, function(err, forum){
      if (forum){
          forum.comments.push({id_group: getOne, nim:req.body.nim, nama:req.body.nama, post:req.body.postings})
              forum.save(function(err){
                if (!err){
                  res.json({success:true, message:'Sukses!'});
                  return forum;
                }else{
                  res.json({success:false, message:'Gagal Cek Kembali!'});
                  return forum;
                }
          });
       return forum;
      }else{
        res.json({success:false, message:'Data Forum Tidak Diketahui!'});
        return forum;        
      }
      return forum;
    });
});

//GET ONE AND DEL Forum *Forum
forumalm.delete('/dellonefrm/:id', function(req, res){
  var getOne = req.params.id;
  Forum.findOneAndRemove({ _id: getOne}, function(err, forums){
    if (err) throw err;
    if (!forums){
      res.json({ success: false, message:'Gagal Forum Tidak Ditemukan'});
      return forums
    }else{
      res.json({ success:true, forums:forums });
      fs.unlink(forums.imgs, function(error) {
                  if (error) {
                   console.log('Error!!', Error);
                  }else{
                    console.log('selesai di hapus!!');
                  }
      }); 
      return forums   
    }
  });  
});

//CEK ONE AND DEL COMMENT *COMMENT THREAD
forumalm.post('/dellonepost', function(req, res){
  console.log(req.body.idPost)
  var getOne = req.body.id_group;
   var options = {$pull: {comments: { _id: req.body.idPost }},};
        Forum.findOneAndUpdate({ _id: getOne, 'comments._id': req.body.idPost }, options, {new: true}, function(err, dataPst){
          if (dataPst){
            res.json({success:true, message:'Sukses Di Hapus'});
            // console.log('succ',dataPst)
            return dataPst;
          }else{
            res.json({success:false, message:'Gagal Menghapus'});
            
            console.log('err',err)
            return dataPst;  
          }
});
});


// PUT ONE AND UPDATE FORUM *FORUM
forumalm.put('/updtfrm', function(req, res){
    var getOne = req.body.id_group;
    Forum.findOne({ _id: getOne}, function(err, edtfrm){
        if (!edtfrm){
          res.json({success:false, message:'Maaf Data Tidak Ditemukan'});   
          return edtfrm;        
        }else{
          // console.log('data', req.files);
          // console.log('data2', req.files.imgs);
            if (req.files.imgs === undefined){
                edtfrm.judul = req.body.judul;
                  // edtfrm.imgs = req.files.imgs['path'];
                  edtfrm.postings = req.body.postings;
                  edtfrm.tag = req.body.tag;
                  edtfrm.save(function(err){
                    if (!err){
                        res.json({success:true, message:'Sukses Di Edit!'});
                        return edtfrm;                             
                    }else{
                      res.json({success:false, message:'Gagal Cek Kembali!'});
                    }
                  });
            }else if(req.files.imgs['extension'] == 'png' || req.files.imgs['extension'] == 'jpg' || req.files.imgs['extension'] == 'jpeg'){
                   fs.unlink(edtfrm.imgs, function(error) {
                          if (error) {
                            console.log('Error!!');
                          }else{
                            console.log('SAVE,selesai di hapus!!');
                            edtfrm.judul = req.body.judul;
                            edtfrm.imgs = req.files.imgs['path'];
                            edtfrm.postings = req.body.postings;
                            edtfrm.tag = req.body.tag;
                            edtfrm.save(function(err){
                              if (!err){
                                  res.json({success:true, message:'Sukses Di Edit!'});
                                  return edtfrm;                             
                              }else{
                                res.json({success:false, message:'Gagal Cek Kembali!'});
                              }
                            });
                            return edtfrm;  
                          }
                  });
            }else{
                res.json({ success: false, message: 'Extensi Harus Png/Jpg/Jpeg'});
                fs.unlink(req.files.imgs['path'], function(error) {
                  if (error) {
                   console.log('Error!!', Error);
                  }else{
                    console.log('selesai di hapus!!');
                  }
                  });
            return edtfrm;
          }
          return edtfrm;
        }
    });
});

forumalm.get('/notifforumalm/:nim', function(req, res){
  var getOne = req.params.nim;
  console.log('from FRM',getOne)
  // User.find({$or:[ {'username': u.username}, {'email': u.email}]} , function(err,user) {
  Forum.find({'comments.nim': getOne}).sort({'comments.$.created_at':-1}).exec(function(err, forums){
    if (err){
      // console.log('gagalDRFRM',err)
      res.json({ success: false, message: 'Data Tidak Ditemukan'});
    }else{
      // Group.findOne({ _id:forums[0]._id}).sort({'postings.created_at':-1}, {new: true}).exec(function(err, grooup1){

        // console.log('SUkses Dr FRM',forums)
        res.json({ success:true, forums:forums});
        // console.log(forums[0]._id)
      // })
    }
  })
});


return forumalm;
};