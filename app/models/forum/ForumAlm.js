var mongoose  = require('mongoose');
var slug      = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
// var CrAlumni  = require('../cr/Cr.Alm.js');
// var CrAlmSchema = CrAlumni.schema;

var ForumSchema = new Schema({
  comments:[{
    id_group:{type:String},
    nim:{type:Number},
    nama:{type:String},
    post:{type:String},
    created_at:{type:Date,default:Date.now}
  }],
  judul: { type:String},
  urlslug:{ type: String, slug: "judul", unique: true},
  postings:{type:String },
  imgs:{ type: String },
  tag:{ type: String },
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
  
});

// ForumSchema.plugin(titlize,{
//     paths: ['addby']
// });

module.exports = mongoose.model('Forum', ForumSchema);