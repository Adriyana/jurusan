var mongoose  = require('mongoose');
var slug      = require('mongoose-slug-generator');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
mongoose.plugin(slug);
var Schema    = mongoose.Schema; 

var PimpinanHasilSurveySchema = new Schema({
  jawaban:[Array],
  addby: {type:String},
  tokens:{type:String},
  id_survey:{type:String},
  created_at:{type:Date,default:Date.now}
});

PimpinanHasilSurveySchema.plugin(titlize,{
    paths: ['addby']
});

module.exports = mongoose.model('PimpinanHasilSurvey', PimpinanHasilSurveySchema);