var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var slug      = require('mongoose-slug-generator');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
mongoose.plugin(slug);

var PimpinanSrvySchema = new Schema({
  
  soal:[Array],
  judul: { type:String},
  deskripsi:{type:String},
  addby: {type:String},
  urlslug:{ type: String, slug: "judul", unique: true},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
  
});

PimpinanSrvySchema.plugin(titlize,{
    paths: ['judul']
});

module.exports = mongoose.model('PimpinanSrvy', PimpinanSrvySchema);