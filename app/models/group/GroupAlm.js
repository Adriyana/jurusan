var mongoose  = require('mongoose');
var slug      = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
// var CrAlumni  = require('../cr/Cr.Alm.js');
// var CrAlmSchema = CrAlumni.schema;

var GroupSchema = new Schema({
  anggota:[{
    nim:{type:Number},
    nama:{type:String},
    created_at:{type:Date,default:Date.now},
    admin:{type:Boolean,default:false}, 
    active: { type: Number, required: true, default: 0},
    addby:{type:Boolean,default:false}
  }],
  postings:[{
    id_group:{type:String},
    nim:{type:Number},
    nama:{type:String},
    created_at:{type:Date,default:Date.now},
    post:{type:String}
  }],
  nama: { type:String},
  urlslug:{ type: String, slug: "nama", unique: true},
  deskripsi:{type:String },
  imgs:{ type: String },
  tag:{ type: String },
  addby: {type:String},
  nim: {type:Number},
  level:{type:String,default: 'Admin'},
  created_at:{type:Date,default:Date.now}
  
});

// GroupSchema.plugin(titlize,{
//     paths: ['addby']
// });

module.exports = mongoose.model('Group', GroupSchema);