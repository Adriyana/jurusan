var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
var slug      = require('mongoose-slug-generator');
mongoose.plugin(slug);

var TdyNewsSchema = new Schema({
  
  judul: { type:String},
  urlslug:{ type: String, slug: "judul", unique: true},
  imgs:{ type: String },
  deskripsi: { type:String },
  bidang: {type:String },
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
  
});

TdyNewsSchema.plugin(titlize,{
    paths: ['judul']
});

module.exports = mongoose.model('TdyNews', TdyNewsSchema);