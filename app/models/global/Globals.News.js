var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
// var slug      = require('mongoose-slug-generator');
// mongoose.plugin(slug);

var GnewSchema = new Schema({
  detail: { 
      deskripsi: { type:String },
      prasyarat: {type:String },
      bidang: {type:String },
      date_on: {type:String},
      date_off: {type:String}
  },
  judul:{type:String},
  urlslug:{type:String},
  urlimage:{type:String },
  perusahaan:{type:String },
  lokasi: { type:String},
  gajih: [Array],
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
 
  
});

GnewSchema.plugin(titlize,{
    paths: ['judul']
});

module.exports = mongoose.model('Gnew', GnewSchema);