var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');

var ScholarSchema = new Schema({
  judul: { type:String},
  urlslug:{ type: String},
  postings:{type:String },
  urlimg:{ type: String },
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
});

ScholarSchema.plugin(titlize,{
    paths: ['judul']
});

module.exports = mongoose.model('Scholar', ScholarSchema);