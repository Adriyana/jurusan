var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');

// var passportLocalMongoose = require('passport-local-mongoose');
// Superadmin Schema

var KerjaSchema = new Schema({
  // id_nim: {type:String, required: true, unique:true},
  nim: {type:Number, required: true, unique:true},
  nama: {type:String},
  company: { type:String},
  web:{ type:String},
  cprofile: { type:String},
  address:{ type:String},
  jenis:{ type:String},
  jabatan:{ type:String},
  lat:{ type:String},
  lng:{ type:String},
  email:{ type:String},
  temporarytoken: {type:String, required: true},
  created_at:{type:Date, default:Date.now}
});

module.exports = mongoose.model('Kerja', KerjaSchema);