var mongoose  = require('mongoose');
var slug      = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema    = mongoose.Schema; 
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
// var CrAlumni  = require('../cr/Cr.Alm.js');
// var CrAlmSchema = CrAlumni.schema;

var SliderSchema = new Schema({
  judul: { type:String},
  deskripsi:{type:String },
  imgs:{ type: String },
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
  
});

SliderSchema.plugin(titlize,{
    paths: ['judul']
});

module.exports = mongoose.model('Slider', SliderSchema);