var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
var namaValidator = [
    validate({
      validator: 'matches',
     arguments: /^(([a-zA-Z]{3,20})+[ ]+([a-zA-Z]{3,20})+)+$/,
        message: 'Name must be at least 3 characters, max 30, no special characters or numbers, must have space in between name.'
    }),
    validate({
        validator: 'isLength',
        arguments: [1, 20],
        message: 'Nama Harus Lebih Dari 3 Huruf'
    })
];

// User E-mail Validator
var emailValidator = [
    validate({
        validator: 'isEmail',
        arguments: /^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/,
        message: 'Its Not Valide EMAIL'
    }),
    validate({
        validator: 'isLength',
        arguments: [3, 40],
        message: 'Email Harus Lebih Dari 3 Huruf'
    })
];

// Username Validator
var usernameValidator = [
    validate({
        validator: 'isLength',
        arguments: [3, 25],
        message: 'Username Harus Lebih Dari 5 Karakter'
    }),
    validate({
        validator: 'isAlphanumeric',
        message: 'Username Harus Angka Dan Huruf'
    })
];

// Password Validator
var passwordValidator = [
    validate({
        validator: 'matches',
        arguments: /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{8,35}$/,
        message: 'Password Harus Berawal Dengan Simbol, 1 Huruf Besar sertakan huruf kecil, Dan Angka. Contoh !@#Kkalumni123'
    }),
    validate({
        validator: 'isLength',
        arguments: [8, 35],
        message: 'Password Harus Melebihi Dari 5 Karakter'
    })
];

var AlumniSchema = new Schema({
  password: { type:String, required: true, validate: passwordValidator, select:false},
  nim: { type:String, required: true, unique:true},
  created_at:{type:Date, default:Date.now},
  verifi:{ type:String, required: true, default:'unverified'},
  active: { type: Boolean, required: true, default: false},
  temporarytoken : {type:String, required: true}
});


AlumniSchema.pre('save', function(next){
  var alumni = this;
  if (!alumni.isModified('password')) return next();
  bcrypt.hash(alumni.password, null, null, function(err, hash){
    if(err) return next(err);
    alumni.password = hash;
    next();
  });
});

AlumniSchema.plugin(titlize,{
    paths: ['nama']
});

AlumniSchema.methods.comparePassword = function(password){
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Alumni', AlumniSchema);