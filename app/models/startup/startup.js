var mongoose  = require('mongoose');
var slug      = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema    = mongoose.Schema; 
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');

var StartUpSchema = new Schema({
  nama: { type:String},
  slug: { type:String},
  bidang:{ type: String },
  tahun:{ type: String },
  website:{ type: String },
  produk:{ type: String },
  addby: { type:String },
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
  
});

StartUpSchema.plugin(titlize,{
    paths: ['nama']
});

module.exports = mongoose.model('StartUp', StartUpSchema);