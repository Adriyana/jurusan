var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 

var AlumniHasilSurveySchema = new Schema({
  jawaban:[Array],
  // hasilawal:[Array],
  addby: {type:String},
  nim: {type:Number},
  id_survey:{type:String},
  created_at:{type:Date,default:Date.now}
});


// AlumniHasilSurveySchema.plugin(titlize,{
//     paths: ['addby']
// });

module.exports = mongoose.model('AlumniHasilSurvey', AlumniHasilSurveySchema);