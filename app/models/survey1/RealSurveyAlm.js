var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var slug      = require('mongoose-slug-generator');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
mongoose.plugin(slug);

var AlumniSrvySchema = new Schema({
  soal:{
    sub:[Array]
  },
  judul: { type:String},
  urlslug:{ type: String, slug: "judul", unique: true},
  deskripsi:{type:String},
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
  
});

AlumniSrvySchema.plugin(titlize,{
    paths: ['judul']
});

module.exports = mongoose.model('AlumniSrvy', AlumniSrvySchema);