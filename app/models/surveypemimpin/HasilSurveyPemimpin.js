var mongoose  = require('mongoose');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
var Schema    = mongoose.Schema; 

var HasilSurveyPemimpinSchema = new Schema({
  jawaban:[Array],
  results: {type:String},
  addby: {type:String},
  jabatan: {type:String},
  email:{type:String},
  hp:{type:Number},
  tokens:{type:String},
  id_survey:{type:String},
  created_at:{type:Date,default:Date.now}
});

HasilSurveyPemimpinSchema.plugin(titlize,{
    paths: ['addby']
});

module.exports = mongoose.model('HasilSurveyPemimpin', HasilSurveyPemimpinSchema);