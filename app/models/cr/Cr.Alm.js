var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');

var CrAlumniSchema = new Schema({
  mhsNiu: { type:String,required: true,unique: true},
  mhsNama: { type:String},
  mhsJenisKelamin: { type:String},
  tempatLahir: { type:String},
  mhsTanggalLahir: { type:String},
  agmrNama: { type:String},
  mhsJumlahSaudara: { type:String},
  mhsAlamatMhs: { type:String},
  mhsNoHp:{ type:String},
  mhsEmail: { type:String},
  mhsProdiKode:{ type:String},
  prodiNamaResmi:{ type:String},
  mhsAngkatan:{ type:String},
  mhsTanggalLulus: { type:String},
  prodi_info: { type: String},
  dosen_pa : {type:String},
  nip_pa : {type:String},
  status : {type:String, default:'Verified'},
  urlslug:{ type: String},
  urlimgs : {type:String, default:'public/1.jpg'},
  created_at:{type:Date, default:Date.now}
});

module.exports = mongoose.model('CrAlumni', CrAlumniSchema);