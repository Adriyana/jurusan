var mongoose  = require('mongoose');
var slug      = require('mongoose-slug-generator');
mongoose.plugin(slug);
var Schema    = mongoose.Schema; 
var bcrypt    = require('bcrypt-nodejs');
var titlize   = require('mongoose-title-case');
var validate  = require('mongoose-validator');
// var CrAlumni  = require('../cr/Cr.Alm.js');
// var CrAlmSchema = CrAlumni.schema;

var EventSchema = new Schema({
  judul: { type:String},
  urlslug:{ type: String},
  postings:{type:String },
  imgs:{ type: String },
  tag:{ type: String },
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
});

EventSchema.plugin(titlize,{
    paths: ['addby']
});

module.exports = mongoose.model('Event', EventSchema);