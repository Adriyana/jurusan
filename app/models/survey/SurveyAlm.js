var mongoose  = require('mongoose');
var Schema    = mongoose.Schema; 
var slug      = require('mongoose-slug-generator');
mongoose.plugin(slug);

var SurveySchema = new Schema({
  soal:[{
    judulsoal:{type:String}
  }],
  options:[Number],
  nama: { type:String},
  urlslug:{ type: String, slug: "nama", unique: true},
  deskripsi:{type:String},
  addby: {type:String},
  nim: {type:Number},
  created_at:{type:Date,default:Date.now}
  
});

// SurveySchema.plugin(titlize,{
//     paths: ['addby']
// });

module.exports = mongoose.model('Survey', SurveySchema);